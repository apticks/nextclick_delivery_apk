package com.kwiky.deliveryapplication.Adapters;

import android.app.AlertDialog;
import android.content.Context;
/*import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;*/
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kwiky.deliveryapplication.Helpers.Fonts.LatoBLack;
import com.kwiky.deliveryapplication.Helpers.TrackingMapRelated.TrackingActivity;
import com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs;
import com.kwiky.deliveryapplication.PojoClass.MyPastOrderModel;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.ActionThingsForOrderReceivedCompletedDelivery;
import static com.kwiky.deliveryapplication.constants.Urls.SingleOrderDetails;

public class OnGoingOrderAdapter extends RecyclerView.Adapter<OnGoingOrderAdapter.MyViewHolder> {

    private List<MyPastOrderModel> modelList;
    private LayoutInflater inflater;
    private Fragment currentFragment;
    private Context context;
    private static int count = 0;
    TextView status;


    String token;
    PreferenceManager preferenceManager;

    public OnGoingOrderAdapter(Context context, List<MyPastOrderModel> modemodelList, final Fragment currentFragment) {
        this.context = context;
        this.modelList = modemodelList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.currentFragment = currentFragment;
        preferenceManager = new PreferenceManager(context);
        token = preferenceManager.getString(TOKEN_KEY);
    }

    public OnGoingOrderAdapter(Context context, List<MyPastOrderModel> modemodelList) {

        this.context = context;
        this.modelList = modemodelList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.currentFragment = currentFragment;
        preferenceManager = new PreferenceManager(context);
        token = preferenceManager.getString(TOKEN_KEY);
    }

    public OnGoingOrderAdapter(List<MyPastOrderModel> modelList) {
        this.modelList = modelList;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView on_going_date, on_going_resturent_name, on_going_resturent_addres, on_going_customer_name, tv_order_no, tv_status, tv_date, tv_time, tv_item, relativetextstatus, tv_tracking_otp, tv_customer_address;
        LatoBLack startTract;
        CardView cardView;
        Button track_vendor,check_items,track_customer;
        public String method;

        public MyViewHolder(View view) {
            super(view);
            track_vendor = (Button) view.findViewById(R.id.track_vendor);
            track_customer = (Button) view.findViewById(R.id.track_customer);
            check_items = (Button) view.findViewById(R.id.check_items);
            tv_order_no = (TextView) view.findViewById(R.id.tv_order_no);
            tv_status = (TextView) view.findViewById(R.id.tv_order_status);
            relativetextstatus = (TextView) view.findViewById(R.id.status);
            tv_tracking_otp = (TextView) view.findViewById(R.id.on_going_otp);
            on_going_date = itemView.findViewById(R.id.on_going_date);
            on_going_resturent_name = (TextView) view.findViewById(R.id.on_going_resturent_name);
            on_going_resturent_addres = (TextView) view.findViewById(R.id.on_going_resturent_addres);
            on_going_customer_name = (TextView) view.findViewById(R.id.on_going_customer_name);
            tv_item = (TextView) view.findViewById(R.id.tv_order_item);
            cardView = view.findViewById(R.id.card_view);
            tv_customer_address = view.findViewById(R.id.on_going_customer_address);
            startTract = view.findViewById(R.id.status);


            //After clicking on Tract Now button for order Received and Complete
            relativetextstatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyPastOrderModel mList = modelList.get(getAdapterPosition());
                    String orderStatusId = mList.getOrderStatusId();
                    String tempId = mList.getId();

                    Map<String, Object> mainData = new HashMap<>();
                    mainData.put("id", tempId);
                    mainData.put("orderstatus_id", orderStatusId);
                    JSONObject json = new JSONObject(mainData);
                    orderRecivedCompleted(view, json, getAdapterPosition());
                    relativetextstatus.setText("Order Completed");
                    notifyItemRangeChanged(getAdapterPosition(), modelList.size());
                    notifyDataSetChanged();
                }
            });


            //for tracking
            track_vendor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TrackingActivity.class);

                    if(modelList.get(getAdapterPosition()).getResturant_lat()!=0.0 && modelList.get(getAdapterPosition()).getResturant_lng()!=0) {
                        //LatLng latLng = new LatLng(modelList.get(getAdapterPosition()).getResturant_lat(),modelList.get(getAdapterPosition()).getResturant_lng()) ;//Or any other value...
                        intent.putExtra("lat", modelList.get(getAdapterPosition()).getResturant_lat() + "".trim());
                        intent.putExtra("long", modelList.get(getAdapterPosition()).getResturant_lng() + "".trim());
                        context.startActivity(intent);
                    }else {
                        UImsgs.showToast(context,"Unable to fetch the location");
                    }
                }
            });
            track_customer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TrackingActivity.class);
                    if(modelList.get(getAdapterPosition()).getCustomer_lat()!=0.0 && modelList.get(getAdapterPosition()).getCustomer_lng()!=0) {
                        //LatLng latLng = new LatLng(modelList.get(getAdapterPosition()).getResturant_lat(),modelList.get(getAdapterPosition()).getResturant_lng()) ;//Or any other value...
                        intent.putExtra("lat", modelList.get(getAdapterPosition()).getCustomer_lat() + "".trim());
                        intent.putExtra("long", modelList.get(getAdapterPosition()).getCustomer_lng() + "".trim());
                        context.startActivity(intent);
                    }else {
                        UImsgs.showToast(context,"Unable to fetch the location");
                    }
                }
            });




        }
    }

    private void orderDetailsFetcher(String order_id, final Button check_items, final Button track_vendor, final Button track_customer, final String otp_str) {
        RequestQueue requstQueue = Volley.newRequestQueue(context);
        StringRequest jsonobj = new StringRequest(Request.Method.GET, SingleOrderDetails+order_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ArrayList<String> itemsList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject dataObject = jsonObject.getJSONObject("data");
                            try{
                                JSONArray orderItemsArray = dataObject.getJSONArray("order_items");
                                for (int i=0 ; i<orderItemsArray.length() ; i++){
                                    JSONObject itemObject = orderItemsArray.getJSONObject(i);
                                    itemsList.add(itemDetailer(itemObject.getString("item_name"),itemObject.getString("quantity")));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            try{
                                JSONArray orderItemsArray = dataObject.getJSONArray("sub_order_items");
                                for (int i=0 ; i<orderItemsArray.length() ; i++){
                                    JSONObject itemObject = orderItemsArray.getJSONObject(i);
                                    itemsList.add(itemDetailer(itemObject.getString("sec_item_name"),itemObject.getString("quantity")));
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }


                            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View viewInflator = layoutInflater.inflate(R.layout.check_items_layout, null);
                            final ListView orderdered_items_list = viewInflator.findViewById(R.id.orderdered_items_list);
                            final Button getOtp = viewInflator.findViewById(R.id.get_otp);
                            final TextView otp = viewInflator.findViewById(R.id.otp);
                            final TextView error_message = viewInflator.findViewById(R.id.error_message);
                            otp.setText(otp_str);


                            ArrayAdapter<String> itemsAdapter =
                                    new ArrayAdapter<String>(context, android.R.layout.simple_list_item_multiple_choice, itemsList);
                            orderdered_items_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);;
                            orderdered_items_list.setAdapter(itemsAdapter);


                            getOtp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    boolean status=true;
                                    for(int i=0;i<orderdered_items_list.getChildCount();i++){
                                        if(!orderdered_items_list.isItemChecked(i)){
                                            status = false;
                                            error_message.setVisibility(View.VISIBLE);
                                            otp.setVisibility(View.GONE);
                                            break;

                                        }else {
                                            status=true;
                                        }
                                    }
                                    if(status) {
                                        error_message.setVisibility(View.GONE);
                                        otp.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                            AlertDialog.Builder alert = new AlertDialog.Builder(context);
               /* alert.setIcon(R.mipmap.ic_launcher);
                alert.setTitle("Receipt");*/
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("Check Items");
                            alert.setView(viewInflator);
                            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {


                                }
                            });
                            alert.show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "" + error, Toast.LENGTH_SHORT).show();

                    }
                }
        );
        jsonobj.setRetryPolicy(new DefaultRetryPolicy(500000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requstQueue.add(jsonobj);
    }


    private void orderRecivedCompleted(final View view, final JSONObject json, final int position) {
        {
            final String data = json.toString();
            Log.d("ord_rec",data);
            Log.d("ord_rec_url",ActionThingsForOrderReceivedCompletedDelivery);
            {
                RequestQueue requstQueue = Volley.newRequestQueue(context);

                StringRequest jsonobj = new StringRequest(Request.Method.POST, ActionThingsForOrderReceivedCompletedDelivery,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("response",response);
                                try {
                                    LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View viewInflator = layoutInflater.inflate(R.layout.custom_toast, null);
                                    TextView constracts = viewInflator.findViewById(R.id.constracts);
                                    constracts.setText("Order Received");
                                    Toast toast = new Toast(context);
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setView(viewInflator);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                                    toast.show();

                                    count = count++;
                                    if (count >= 1) {
                                        remove(position);
                                    }
                                    JSONObject jsonResult = new JSONObject(response.toString());
//                                    String status = jsonResult.getString("status");
                                    //JSONArray roles = jsonResult.getJSONArray("Roles");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "" + error, Toast.LENGTH_SHORT).show();

                            }
                        }
                ) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override

                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("X_AUTH_TOKEN", token);
                        Log.d("X_AUTH_TOKEN", token);
                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return data == null ? null : data.getBytes("utf-8");

                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                };
                jsonobj.setRetryPolicy(new DefaultRetryPolicy(500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requstQueue.add(jsonobj);
            }
        }
    }

    @Override
    public OnGoingOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_on_going_order_rv, parent, false);
        context = parent.getContext();
        return new OnGoingOrderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MyPastOrderModel mList = modelList.get(position);
        if (mList.getOrderStatusId().equals("")) {
            holder.tv_status.setText(context.getResources().getString(R.string.pending));
            holder.tv_status.setTextColor(context.getResources().getColor(R.color.Red));

            holder.relativetextstatus.setVisibility(View.GONE);
            holder.relativetextstatus.setText(mList.getOrderStatus());
        } else {
            holder.tv_status.setText(context.getResources().getString(R.string.confirm));
            holder.relativetextstatus.setVisibility(View.VISIBLE);
            holder.tv_status.setTextColor(context.getResources().getColor(R.color.Green));
            holder.relativetextstatus.setText(mList.getOrderStatus());
        }
        holder.on_going_resturent_name.setText(mList.getRestaurantName());
        holder.on_going_resturent_addres.setText(mList.getRestaurantAddress());
        holder.on_going_customer_name.setText(mList.getCustomerName());
        holder.on_going_date.setText(mList.getCreated_at());
        holder.tv_order_no.setText(mList.getOrder_number());
        holder.tv_tracking_otp.setText(mList.getOtp());

        holder.tv_customer_address.setText(mList.getDelivery_address());
        holder.check_items.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderDetailsFetcher(mList.getOrder_id(),holder.check_items,holder.track_vendor,holder.track_customer,mList.getOtp());
            }
        });

    }

    private String itemDetailer(String item,String qty){
        return "Item : "+item+"\n"+"Qty : "+qty;
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public void remove(int position) {
        modelList.remove(position);
        notifyItemRemoved(position);

        count = 0;
    }
}
