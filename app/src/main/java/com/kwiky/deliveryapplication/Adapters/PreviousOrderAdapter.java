package com.kwiky.deliveryapplication.Adapters;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kwiky.deliveryapplication.Helpers.Fonts.LatoBLack;
import com.kwiky.deliveryapplication.PojoClass.MyPastOrderModel;
import com.kwiky.deliveryapplication.R;

import java.util.ArrayList;
import java.util.List;

public class PreviousOrderAdapter extends RecyclerView.Adapter<PreviousOrderAdapter.ViewHolder> {
    List<MyPastOrderModel> myPreviousOrderModelList = new ArrayList<>();
    public PreviousOrderAdapter(List<MyPastOrderModel> myPreviousOrderModelList) {
        this.myPreviousOrderModelList = myPreviousOrderModelList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_previous_order_adapter, parent, false);

        return new PreviousOrderAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MyPastOrderModel myPastOrderModel = myPreviousOrderModelList.get(position);

        holder.tv_order_no.setText(myPastOrderModel.getId());
        holder.tv_order_price.setText("₹"+myPastOrderModel.getPrice());
        holder.customer_name.setText(myPastOrderModel.getCustomerName());
        holder.vendor.setText(myPastOrderModel.getRestaurantName());
    }
    @Override
    public int getItemCount() {
        return myPreviousOrderModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LatoBLack tv_order_no,customer_name,tv_order_date,tv_order_time,tv_order_item;
        TextView tv_order_price,vendor;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_order_no = itemView.findViewById(R.id.tv_order_no);
            tv_order_price  = itemView.findViewById(R.id.tv_order_price);
            customer_name = itemView.findViewById(R.id.customer_name);
            vendor = itemView.findViewById(R.id.vendor);
        }
    }
}
