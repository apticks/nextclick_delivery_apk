package com.kwiky.deliveryapplication.Adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.kwiky.deliveryapplication.Helpers.Fonts.LatoBLack;
import com.kwiky.deliveryapplication.PojoClass.OrderListModelClass;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.constants.IConstant;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.ToAcceptRequestedOrders;

public class CurrentOrderListAdapter extends RecyclerView.Adapter<CurrentOrderListAdapter.MyViewHolder> {
    private Context mContext;
    private Date oneWayTripDate;
    List<OrderListModelClass> orderListArrayList = new ArrayList<>();
    String token;
    PreferenceManager preferenceManager;


    public CurrentOrderListAdapter() {
    }

    public CurrentOrderListAdapter(List<OrderListModelClass> orderListArrayList) {
        this.orderListArrayList = orderListArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_current_order_list_adapter, parent, false);

        mContext = parent.getContext();
       preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        return new CurrentOrderListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        OrderListModelClass orderListModelClass = orderListArrayList.get(position);

        holder.tvOrderNo.setText(orderListModelClass.getOrderNo());
        holder.text_resturent_name.setText(orderListModelClass.getResturant());
        holder.tvReciverName.setText(orderListModelClass.getCustomer());
        holder.tvRecivernAddress.setText(orderListModelClass.getCustomerAddress());
        holder.restaurant_address.setText(orderListModelClass.getResturantAddress());
//        holder.tv_date_time.setText(orderListModelClass.getCreated_at());
        String DateTime = orderListModelClass.getCreated_at();

        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss aa");
        try {
            // parse input
            oneWayTripDate = input.parse(DateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tv_date_time.setText(output.format(oneWayTripDate));
    }
    @Override
    public int getItemCount() {
        return orderListArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LatoBLack tvOrderNo, tvReciverName, tvRecivernAddress, restaurant_address;
        TextView reject_order, accept_order, tv_date_time, text_resturent_name;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvOrderNo = itemView.findViewById(R.id.tv_order_no);
            text_resturent_name = itemView.findViewById(R.id.text_resturent_name);
            tvReciverName = itemView.findViewById(R.id.tv_recivername);
            tvRecivernAddress = itemView.findViewById(R.id.tv_recivernaddress);
            reject_order = itemView.findViewById(R.id.reject_order);
            accept_order = itemView.findViewById(R.id.accept_order);
            tv_date_time = itemView.findViewById(R.id.tv_date_time);
            restaurant_address = itemView.findViewById(R.id.restaurant_address);

            accept_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OrderListModelClass orderListModelClass = orderListArrayList.get(getAdapterPosition());
                    String tempId = orderListModelClass.getId();

                    Map<String, Object> mainData = new HashMap<>();
                    mainData.put("id", tempId);
                    mainData.put("status", "2");
                    JSONObject json = new JSONObject(mainData);
                    orderAcceptRejectAPI(view, json, getAdapterPosition(), mContext.getString(R.string.order_accept));
                    notifyDataSetChanged();
                }
            });
            //Rejecting
            reject_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OrderListModelClass orderListModelClass = orderListArrayList.get(getAdapterPosition());
                    String tempId = orderListModelClass.getId();
                    Map<String, Object> mainData = new HashMap<>();
                    mainData.put("id", tempId);
                    mainData.put("status", "");
                    JSONObject json = new JSONObject(mainData);
                    orderAcceptRejectAPI(view, json, getAdapterPosition(), mContext.getString(R.string.order_reject));
                    notifyDataSetChanged();
                }
            });
        }
    }

    private void orderAcceptRejectAPI(final View viewSnack, JSONObject json, final int position, final String order) {
        final String data = json.toString();
        {
            RequestQueue requstQueue = Volley.newRequestQueue(mContext);

            StringRequest jsonobj = new StringRequest(Request.Method.POST, ToAcceptRequestedOrders, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        //LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_current_order_list_adapter, parent, false);
                        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View viewInflator = layoutInflater.inflate(R.layout.custom_toast, null);

                        TextView constracts = viewInflator.findViewById(R.id.constracts);
                        constracts.setText(order);
                        Toast toast = new Toast(mContext);
                        toast.setDuration(Toast.LENGTH_SHORT);
                        toast.setView(viewInflator);
                        toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                        remove(position);
                        //Vibrate
                        if (order.equals(mContext.getString(R.string.order_accept))) {
                            vibratePhone(true);
                        } else {
                            vibratePhone(false);
                        }
                                /*OnGoingOrderList onGoingOrderList = new OnGoingOrderList(mContext);
                                onGoingOrderList.makeGetOrderRequest();*/

                                /*Snackbar.make(viewSnack, response, Snackbar.LENGTH_LONG)
                                        .setAction("OK", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //Login(v);
                                            }
                                        }).show();*/
                        JSONObject jsonResult = new JSONObject(response.toString());
                        //String status = jsonResult.getString("status");
                        //JSONArray roles = jsonResult.getJSONArray("Roles");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Toast.makeText(mContext, "Some Error Occured", Toast.LENGTH_SHORT).show();
                            Snackbar.make(viewSnack, IConstant.OOPS, Snackbar.LENGTH_LONG)
                                    .setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            //Login(v);
                                        }
                                    }).show();
                        }
                    }
            ) {
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override

                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("X_AUTH_TOKEN", token);
                    return params;
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return data == null ? null : data.getBytes("utf-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            };
            jsonobj.setRetryPolicy(new DefaultRetryPolicy(500000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requstQueue.add(jsonobj);


        }
    }

    //Vibrate Phone For One Second
    private void vibratePhone(boolean booleanTemp) {
        ((Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE))
                .vibrate(200);

        /*Vibrator vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(200);*/

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.URI_COLUMN_INDEX);
            Ringtone r = RingtoneManager.getRingtone(mContext, notification);

            //Media Player
            MediaPlayer mMediaPlayer = new MediaPlayer();

            if (booleanTemp) {
                mMediaPlayer = MediaPlayer.create(mContext, R.raw.acceptbeepsound);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.start();
            } else {
                mMediaPlayer = MediaPlayer.create(mContext, R.raw.rejectbeepsound);
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mMediaPlayer.setLooping(false);
                mMediaPlayer.start();
            }
            //r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //remove from Array List
    public void remove(int position) {
        orderListArrayList.remove(position);
        notifyItemRemoved(position);
    }

}
