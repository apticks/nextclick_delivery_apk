package com.kwiky.deliveryapplication.constants;

public interface Urls {
    //String BASE_URL = "http://cineplant.com/next/";//testing
    //String BASE_URL = "http://cineplant.com/nextclick/";//production
    //String BASE_URL = "http://foodynest.com/nextclick/";//main production
    String BASE_URL = "http://nextclick.apticks.com/";//main production
//    String BASE_URL = "http://cineplant.com/flymengo/";

    String Login = BASE_URL + "auth/api/auth/login";
    String UserDetails = BASE_URL + "user/master/user_details/";

    String Registration = BASE_URL + "betting/betting_auth/demo";
    String UploadImage = BASE_URL + "team_create";
    String EnquiryType = BASE_URL + "EnquiryType";
    String SourceOfEnquiry = BASE_URL + "SourceOfEnquiry";
    String SaleType = BASE_URL + "SaleType";
    String Designations = BASE_URL + "Designations";
    String States = BASE_URL + "States";

    public String ForgotPassword = BASE_URL+"auth/api/auth/forgot_password";


    String SingleOrderDetails = BASE_URL + "user/food/Order/";//order_id needed
    String DiorderRequestFormRestaurants = BASE_URL + "user/food/FoodDealOrdRequests/1";
    String ToAcceptRequestedOrders = BASE_URL + "user/food/FoodDealOrdRequest";
    String AcceptedOrdersRequestFormRestaurants = BASE_URL + "user/food/FoodDealOrdRequests/2";
    String CompletedOrdersDetails = BASE_URL + "user/food/FoodDealOldHistory";
    String ActionThingsForOrderReceivedCompletedDelivery = BASE_URL + "user/food/FoodDealOrdReceived";
    String DeliveryPersonStatusU = BASE_URL + "user/delivery/DeliveryBoyStatus/u";
    String DeliveryPersonStatusR = BASE_URL + "user/delivery/DeliveryBoyStatus/r";
    String SendLatLongstatus = BASE_URL + "user/delivery/DeliveryBoyLatLong";
    String PROFILE = BASE_URL + "user/master/user_details/";

    //==============================================================================
    public String REG_BASE_URL = "http://cineplant.com/nextclick_reg/";
    public String PROFILE_PIC_R = REG_BASE_URL + "api/index/profile_pic/r";
    //=========================================================================

    String Roles = BASE_URL + "Roles";
    String Enquiry = BASE_URL + "Enquiry" + "?enquiry_id=";
    String EnquirySubmitt = BASE_URL + "Enquiry";
    String EnquiryUpdate = BASE_URL + "EnquiryUpdate";
    String Customers = BASE_URL + "Customers";
    String district_id = "?district_id=";
    String Enquirys = BASE_URL + "Enquirys" + "?user_id=";
    String Activity = BASE_URL + "Activity";
    String Warranty = BASE_URL + "Warranty";
    String ChangeStatusOfPayment = BASE_URL + "change_status_of_payment";
    String PaymentConfirmation = BASE_URL + "payment_confirmation";
    String ScoreDetails = BASE_URL + "score_details";

    String Location_List = BASE_URL + "agent/agent/City_Location";
    String ImageBitmapData = "sdfskljdsfsdkflkd;lsfk;lsdkfl;dkl;fkdl;skfl;dkflkdjfkdjfkjdkfjkdjfkdjfkjdkfjkdjfkdjfkjdkfjkdjfkdjfkjdkjfuoeiroi";
    String Update_Profile = "update_profile";
    String Update_Password = "update_password";
    String Forgot_Password = "forgot_password";
    String Property_History = "property_history";
    String Residential = "property_filter/residential";
    String Commercial = "property_filter/commercial";
    String State_List = "state_list";
    String City_List = "city_list";
    String City_Zone_List = "city_zone_list";
    String City_Location = "city_location";
    String Posted_By = "posted_by";
    String Category_Name = "category_name";
    String Selected_Property = "selected_property";
}