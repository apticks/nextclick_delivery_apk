package com.kwiky.deliveryapplication.constants;

public interface IConstant {

    final static String storedUserName = null;
    String param_email ="email";
    String param_identity ="identity";
    String param_location ="LOCATION_NAME";
    String param_role_id ="role_id";
    String param_request_id="request_id";

    String param_firstName ="first_name";
    String param_lastName = "last_name";
    String param_password ="password";
    String param_mobile ="last_name";
    String param_location_id = "localtion_id";
    String param_state_id = "state_id";
    String param_city_id = "city_id";
    String param_next_date = "next_date";

    String activity_id = "activity_id";
    String param_customer_id ="customer_id";
    String param_activity_status ="activity_status";
    String param_remarks="remarks";
    String param_delivery_boy_status ="delivery_boy_status";

    String RUPEES_SYMBOL="\u20B9 ";

    //Enquiry Submitt

    String enquiry_type="enquiry_type";
    String source_of_enquiry="source_of_enquiry";
    String enquiry_id = "enquiry_id";

    String sale_type_id = "sale_type_id";
    String customer_name="customer_name";
    String c_person="c_person";


    String c_designation="c_designation";
    String c_mobile="c_mobile";
    String c_whatsapp_number = "c_whatsapp_number";
    String c_email="c_email";
    String request_id="request_id";
    String noRequestFound = "No Request Found";
    String ac_person="ac_person";


    String ac_designation="ac_designation";
    String ac_mobile="ac_mobile";
    String ac_email="ac_email";
    String office_address="office_address";
    String office_state="office_state";
    String office_district="office_district";
    String office_city="office_city";
    String office_pincode="office_pincode";
    String office_gstin="office_gstin";
    String office_iecode="office_iecode";

    String factory_address="factory_address";
    String factory_state ="factory_state";
    String factory_district = "factory_district";
    String factory_city="factory_city";
    String factory_pincode="factory_pincode";
    String business_type="business_type";
    String business_sub_category="business_sub_category";
    String others = "others";
    String mill_capacity="mill_capacity";
    String capacity_required="capacity_required";
    String machine_required="machine_required";
    String machine_description = "machine_description";
    String make="make";
    String model_type="model_type";
    String model_ultima_id = "model_ultima_id";
    String model_suffix = "model_suffix";
    String chute="chute";
    String chute_type="chute_type";
    String chute_exp="chute_exp";
    String units = "units";
    String price = "price";
    String delivery_days = "delivery_days";
    String advance_payment = "advance_payment";
    String payment_type="payment_type";
    String additional_info="additional_info";
    String next_follow_date="next_follow_date";
    String warranty_id="warranty_id";
    String ter1 = "ter1";
    String ter2 = "ter2";
    String ter3 = "ter3";
    String clearing_exp_at_port = "clearing_exp_at_port";
    String destination = "destination";
    String gst = "gst";
    String review_comment = "review_comment";
    String requestfile = "requestfile";
    String user_id="user_id";


    //Please check your network connection
    String incorrecUserNamePass = "Please Check Email Id and Password";
    String internet_connnection="Please check your network connection";
    String sucessSubmitt= "Sucessfully Subimtted";
    String sucess = "Requested Successfully";
    String sucessEdited = "Edited Successfully ";

    String OOPS = "OOPS! Something went wrong please try again";
    String someErrorOccuredPDF= "Some Error Occured While Creating PDF";
    String someErrorOccured= "Some Error Occured";
    String dataNotFound = "Data Not Found";

    int MY_SOCKET_TIMEOUT_MS =10000;

}
