package com.kwiky.deliveryapplication.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.kwiky.deliveryapplication.ActivitiesActivity.DashBoardActivity;
import com.kwiky.deliveryapplication.Helpers.netwrokHelpers.VolleyHelper;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.ui.currentOrders.CurrentOrderList;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;

import static com.android.volley.VolleyLog.TAG;

public class FoodJobDetailServices extends Service {
    private VolleyHelper volleyHelper;
    private PreferenceManager mPreferenceManager;
    public static Timer timer;
    IBinder binder;      // interface for clients that bind
    Context mContext;
    DashBoardActivity dashBoardActivity;

    public FoodJobDetailServices(DashBoardActivity mContext) {
        dashBoardActivity = mContext;
    }

    public FoodJobDetailServices() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
        volleyHelper = new VolleyHelper(this);
        //volleyHelper = new VolleyHelper(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //  Log.d("run: ", "service is running!");
                try {
                    //

                    volleyHelper.getFoodJobDetail();

                    /*OnGoingOrderList onGoingOrderList = new OnGoingOrderList(mContext);
                    onGoingOrderList.makeGetOrderRequest();*/

                    Log.v(TAG, "Inside Food Food Job Detail Services");

                    //Toast.makeText(mContext, "Toast", Toast.LENGTH_SHORT).show();

                    /*DashBoardActivity dashBoardActivity = new DashBoardActivity();
                    dashBoardActivity.toastText();*/

                }/* catch (JSONException e) {
                    e.printStackTrace();
                }*///
                catch (Exception e) {
                    e.printStackTrace();
                    Log.v(TAG, String.valueOf(e));
                    //Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                   /*  DashBoardActivity dashBoardActivity = new DashBoardActivity();
                     dashBoardActivity.toastText();*/
                }
            }
        }, 0, 9000);
        return START_STICKY;
    }


    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //stopSelf();
    }
}
