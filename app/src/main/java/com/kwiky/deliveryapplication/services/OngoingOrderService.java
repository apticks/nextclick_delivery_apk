package com.kwiky.deliveryapplication.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;


import com.kwiky.deliveryapplication.ActivitiesActivity.DashBoardActivity;
import com.kwiky.deliveryapplication.ui.currentOrders.OnGoingOrderList;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by Chanchal Shakti on 11/11/'19.
 */

public class OngoingOrderService extends Service {
    //private VolleyHelper volleyHelper;
    private PreferenceManager mPreferenceManager;
    public static Timer timer;
    IBinder binder;      // interface for clients that bind
    Context mContext ;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        //Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
        //volleyHelper = new VolleyHelper(this);
        mPreferenceManager = new PreferenceManager(this);
        mContext = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                //  Log.d("run: ", "service is running!");
                try {
                    //volleyHelper.getNewJobRequest(mPreferenceManager.getString("sp_id"));
                   /* OnGoingOrderList onGoingOrderList = new OnGoingOrderList(mContext);
                    onGoingOrderList.makeGetOrderRequest();*/

                    Log.v(TAG,"Inside Services");

                    //Toast.makeText(mContext, "Toast", Toast.LENGTH_SHORT).show();

                    /*DashBoardActivity dashBoardActivity = new DashBoardActivity();
                    dashBoardActivity.toastText();*/



                }/* catch (JSONException e) {
                    e.printStackTrace();
                }*/
                 catch (Exception e) {
                    e.printStackTrace();
                     Log.v(TAG,String.valueOf(e));
                     //Toast.makeText(mContext, String.valueOf(e), Toast.LENGTH_SHORT).show();
                   /*  DashBoardActivity dashBoardActivity = new DashBoardActivity();
                     dashBoardActivity.toastText();*/
                }
            }
        }, 0, 9000);
        return START_STICKY;
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Override
    public void onDestroy() {
        stopSelf();
    }
}
