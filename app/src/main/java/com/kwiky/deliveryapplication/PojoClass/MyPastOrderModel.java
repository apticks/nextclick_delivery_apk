package com.kwiky.deliveryapplication.PojoClass;

public class MyPastOrderModel {
    private String customerName;
    private String id;
    private String order_id;
    private String order_number;
    private String restaurantName;
    private String restaurantAddress;
    private String price;
    private String status;
    private String delivery_address;
    private String otp;
    private String orderStatus;
    private String orderStatusId;
    private String created_at;
    private Double resturant_lat;
    private Double resturant_lng;
    private Double customer_lat;
    private Double customer_lng;


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(String orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getCustomerName(){
        return customerName;
    }

    public String getId(){
        return id;
    }

    public String getRestaurantAddress(){
        return restaurantAddress;
    }

    public String getRestaurantName(){
        return restaurantName;
    }

    public String getStatus(){
        return status;
    }

    public String getDelivery_address(){
        return delivery_address;
    }


    public Double getResturant_lat() {
        return resturant_lat;
    }

    public void setResturant_lat(Double resturant_lat) {
        this.resturant_lat = resturant_lat;
    }

    public Double getResturant_lng() {
        return resturant_lng;
    }

    public void setResturant_lng(Double resturant_lng) {
        this.resturant_lng = resturant_lng;
    }

    public Double getCustomer_lat() {
        return customer_lat;
    }

    public void setCustomer_lat(Double customer_lat) {
        this.customer_lat = customer_lat;
    }

    public Double getCustomer_lng() {
        return customer_lng;
    }

    public void setCustomer_lng(Double customer_lng) {
        this.customer_lng = customer_lng;
    }
}
