package com.kwiky.deliveryapplication.Helpers.TrackingMapRelated;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
