package com.kwiky.deliveryapplication.Helpers.netwrokHelpers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kwiky.deliveryapplication.ActivitiesActivity.DashBoardActivity;
import com.kwiky.deliveryapplication.Adapters.CurrentOrderListAdapter;
import com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs;
import com.kwiky.deliveryapplication.PojoClass.OrderListModelClass;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.ui.currentOrders.CurrentOrderList;
import com.kwiky.deliveryapplication.utilities.CustomerServicesApp;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;


//import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.NotificationManager.IMPORTANCE_LOW;
import static com.android.volley.VolleyLog.TAG;
import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.*;


/**
 * Created by Madhu on 17/06/17.
 */
public class VolleyHelper {
    private static final String TAG = "Volley";
    private Context mContext;
    private PreferenceManager mPreferenceManager;
    String token;


    public VolleyHelper(Context context) {
        this.mContext = context;
        mPreferenceManager = new PreferenceManager(context);
        token = mPreferenceManager.getString(TOKEN_KEY);
    }

    public void signIn(JSONObject loginJson) {}/*{
        Log.d(TAG, "signIn: API" + Constants.BASE_URL + Constants.SIGNIN_URL + " " + loginJson);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.SIGNIN_URL, loginJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {
                        //{"status":true,"data":{"id":"26","name":"madhu","mobile":"9036849397","email":"madhu@infoskaters.com","icon":"","status":"","job_completed":"0","services":[],
                        // "rating":0},"token":"e5d50e9fe8333be196ce","message":"Service Provider is Successfully Loggedin!!!"}
                        Log.d(TAG, "signIn RESPONSE" + loginResponse.toString());
                        //{"status":false,"data":[],"message":"Invalid UserName or Password"}
                        try {
                            if (loginResponse.getString("status").equals("false")) {
                                EventBus.getDefault().post(new LoginEvent(false, loginResponse.getString("message"), loginResponse));
                            } else {
                                JSONObject jsonObject = loginResponse.getJSONObject("data");
                                mPreferenceManager.putString("login_response", jsonObject.toString());
                                mPreferenceManager.putString("sp_id", jsonObject.getString("id"));
                                mPreferenceManager.putString("token", loginResponse.getString("token"));
                                mPreferenceManager.putString("mobile", jsonObject.getString("mobile"));
                                mPreferenceManager.putString("email", jsonObject.getString("email"));
                                mPreferenceManager.putString("sp_icon", jsonObject.getString("icon"));
                                mPreferenceManager.putString("sp_name", jsonObject.getString("name"));
                                mPreferenceManager.putString("is_delivery_staff", jsonObject.getString("is_delivery_staff"));

                                pushFcmToken();
                                NServicesSingleton.getInstance().setToken(loginResponse.getString("token"));
                                EventBus.getDefault().post(new LoginEvent(true, loginResponse.getString("message"), loginResponse));

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new LoginEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void generateOtp(final JSONObject loginJson) {}/*{
        Log.d(TAG, "generateOtp: API" + Constants.BASE_URL + Constants.GENERATE_OTP + " " + loginJson);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.GENERATE_OTP, loginJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {
                        Log.d(TAG, "generateOtp: RESPONSE" + loginResponse);

                        try {
                            JSONObject jsonObject = loginResponse.getJSONObject("data");

                            EventBus.getDefault().post(new SignUpOtpEvent(true, jsonObject.getString("otp") + loginResponse.getString("message"), loginResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new SignUpOtpEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new SignUpOtpEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new SignUpOtpEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getTodaysJobsList(String spId) {}/*{

        CustomerServicesApp.getInstance().getRequestQueue().getCache().remove(Constants.BASE_URL + Constants.TODAY_JOBS_LIST_URL + spId);


        Log.d(TAG, "getTodaysJobsList: API " + Constants.BASE_URL + Constants.TODAY_JOBS_LIST_URL + spId);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.TODAY_JOBS_LIST_URL + spId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getTodaysJobsList: RESPONSE " + spResponse.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<TodaysJobModel>>() {
                        }.getType();
                        ArrayList<TodaysJobModel> todaysJobModels = null;
                        try {
                            todaysJobModels = gson.fromJson(spResponse.getJSONArray("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new TodaysJobEvent(true, todaysJobModels));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new TodaysJobEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new TodaysJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getFutureJobsList(String spId) {}/*{
        Log.d(TAG, "getFutureJobsList: API " + Constants.BASE_URL + Constants.FUTURE_JOBS_LIST_URL + spId);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.FUTURE_JOBS_LIST_URL + spId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getFutureJobsList: RESPONSE" + spResponse.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<TodaysJobModel>>() {
                        }.getType();
                        ArrayList<TodaysJobModel> todaysJobModels = null;
                        try {
                            todaysJobModels = gson.fromJson(spResponse.getJSONArray("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new FutureJobEvent(true, todaysJobModels));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new FutureJobEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new FutureJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //   params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getCompletedJobsList(String spId) {}/*{
        Log.d(TAG, "getCompletedJobsList: API: " + Constants.BASE_URL + Constants.COMPLETED_JOBS_LIST_URL + spId);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.COMPLETED_JOBS_LIST_URL + spId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getCompletedJobsList: RESPONSE " + spResponse.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<TodaysJobModel>>() {
                        }.getType();
                        ArrayList<TodaysJobModel> todaysJobModels = null;
                        try {
                            todaysJobModels = gson.fromJson(spResponse.getJSONArray("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new CompleteJobEvent(true, todaysJobModels));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new CompleteJobEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new CompleteJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //   params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getNewJobRequest(String spId) throws JSONException {}/*{
        JSONObject json = new JSONObject();

        if (mPreferenceManager.getString("user_lat") != null) {
            json.put("sp_id", mPreferenceManager.getString("sp_id"));
            json.put("latitude", mPreferenceManager.getString("user_lat"));
            json.put("longitude", mPreferenceManager.getString("user_long"));
        }
        Log.d(TAG, "getNewJobRequest: API " + Constants.BASE_URL + Constants.GET_NEW_JOB_REQUEST + " " + json);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.GET_NEW_JOB_REQUEST, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getNewJobRequest: RESPONSE " + spResponse.toString());


                        try {
                            JSONArray otherRequests = spResponse.getJSONObject("data").getJSONArray("other");
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<TodaysJobModel>>() {
                            }.getType();
                            ArrayList<TodaysJobModel> todaysJobModels = null;
                            todaysJobModels = gson.fromJson(otherRequests.toString(), type);


                            //cab
                            JSONArray cabRequests = spResponse.getJSONObject("data").getJSONArray("cab");
                            Gson gsonCab = new Gson();
                            Type cabType = new TypeToken<ArrayList<CabRequestModel>>() {
                            }.getType();
                            ArrayList<CabRequestModel> cabRequestModels = null;
                            cabRequestModels = gsonCab.fromJson(cabRequests.toString(), cabType);

                            if (todaysJobModels.size() > 0) {
                                EventBus.getDefault().post(new NewJobRequestEvent(true, "", todaysJobModels));
                            } else if (cabRequestModels.size() > 0) {
                                EventBus.getDefault().post(new NewJobCabRequestEvent(true, "", cabRequestModels));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new FutureJobEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new FutureJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void acceptCabNewJob(JSONObject jsonObject, final boolean status) {}/*{
        Log.d(TAG, "acceptCabNewJob: API " + Constants.BASE_URL + Constants.CAB_JOB_REQUEST_ACCEPT);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.CAB_JOB_REQUEST_ACCEPT, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        Log.d(TAG, "acceptCabNewJob: RESPONSE " + jsonResponse.toString());
                        try {
                            if (status)
                                EventBus.getDefault().post(new JobAcceptEvent(true, jsonResponse.getString("message")));
                            else
                                EventBus.getDefault().post(new JobRejectEvent(true, jsonResponse.getString("message")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //{"status":true,"data":[],"message":"Job Request Rejected Successfully"}
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new JobAcceptEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new JobAcceptEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //    Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void rejectCabNewJob(JSONObject jsonObject, final boolean status) {}/*{
        Log.d(TAG, "rejectCabNewJob: API " + Constants.BASE_URL + Constants.CAB_JOB_REQUEST_REJECT + " " + jsonObject);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.CAB_JOB_REQUEST_REJECT, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        Log.d(TAG, "rejectCabNewJob: RESPONSE " + jsonResponse.toString());
                        try {
                            if (status)
                                EventBus.getDefault().post(new JobAcceptEvent(true, jsonResponse.getString("message")));
                            else
                                EventBus.getDefault().post(new JobRejectEvent(true, jsonResponse.getString("message")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //{"status":true,"data":[],"message":"Job Request Rejected Successfully"}

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new JobAcceptEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new JobAcceptEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //    Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void acceptRejectNewJobEvent(JSONObject jsonObject, final boolean status) {}/*{
        Log.d(TAG, "acceptRejectNewJobEvent: API " + Constants.BASE_URL + Constants.JOB_REQUEST_ACCEPT_REJECT);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.JOB_REQUEST_ACCEPT_REJECT, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        Log.d(TAG, "acceptRejectNewJobEvent: RESPONSE " + jsonResponse.toString());
                        try {
                            if (status)
                                EventBus.getDefault().post(new JobAcceptEvent(true, jsonResponse.getString("message")));
                            else
                                EventBus.getDefault().post(new JobRejectEvent(true, jsonResponse.getString("message")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //{"status":true,"data":[],"message":"Job Request Rejected Successfully"}

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new JobAcceptEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new JobAcceptEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //    Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void updateJobStatus(String bookingId, final String status) throws JSONException {}/*{
        JSONObject json = new JSONObject();

        if (mPreferenceManager.getString("sp_id") != null) {
            json.put("sp_id", mPreferenceManager.getString("sp_id"));
            json.put("booking_id", bookingId);
            json.put("status", status);
            json.put("extra",mPreferenceManager.getString("extra"));
            json.put("extra_note",mPreferenceManager.getString("extra_note"));
        }

        JSONObject locationJson = new JSONObject();
        if (mPreferenceManager.getString("user_lat") != null) {
            locationJson.put("latitude", mPreferenceManager.getString("user_lat"));
            locationJson.put("longitude", mPreferenceManager.getString("user_long"));
            json.put("location", locationJson);
        }
        Log.d(TAG, "updateJobStatus: API " + Constants.BASE_URL + Constants.UPDATE_JOB_STATUS + " " + json);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.UPDATE_JOB_STATUS, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        //{"status":true,"data":[],"message":"Service provider status updates successfully"}
                        Log.d("Accept Reject RESPONSE:", spResponse.toString());

                        try {
                            EventBus.getDefault().post(new JobStartEndEvent(true, spResponse.getString("message"), status));
                            ;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new FutureJobEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new FutureJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //    Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void onOffJOb(JSONObject json) throws JSONException {}/*{
        JSONObject locationJson = new JSONObject();
        locationJson.put("latitude", NServicesSingleton.getInstance().getMycurrentLatitude());
        locationJson.put("longitude", NServicesSingleton.getInstance().getMycurrentLongitude());
        json.put("location", locationJson);


        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.UPDATE_JOB_STATUS, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        //{"status":true,"data":[],"message":"Service provider status updates successfully"}
                        Log.d("Accept Reject API:", spResponse.toString());

                        try {
                            EventBus.getDefault().post(new OnOffJobEvent(true, spResponse.getString("message")));
                            ;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new OnOffJobEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new OnOffJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //    Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void calculateDistance(String url) {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        Log.d(TAG, jsonObject.toString());
                        JSONArray array = null;
                        try {
                            array = jsonObject.getJSONArray("routes");
                            JSONObject routes = array.getJSONObject(0);
                            JSONArray legs = routes.getJSONArray("legs");
                            JSONObject steps = legs.getJSONObject(0);
                            JSONObject distance = steps.getJSONObject("distance");
                            String parsedDistance = distance.getString("text");

                            EventBus.getDefault().post(new MapDistanceEvent(true, "", parsedDistance));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new MapDistanceEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new TodaysJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                Log.d("token: ", mPreferenceManager.getString("token"));
                params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void signUp(final JSONObject signUpJson) {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.SIGNUP_URL, signUpJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject signUpResponse) {
                        Log.d(TAG, signUpResponse.toString());
                        //{"status":false,"data":[],"message":"Mobile No aleady Exist"}
                        try {
                            if (signUpResponse.getBoolean("status")) {
                                JSONObject jsonObject = signUpResponse.getJSONObject("data");
                                mPreferenceManager.putString("login_response", jsonObject.toString());
                                mPreferenceManager.putString("mobile", jsonObject.getString("mobile"));
                                mPreferenceManager.putString("email", jsonObject.getString("email"));
                                mPreferenceManager.putString("sp_id", jsonObject.getString("id"));
                                //      mPreferenceManager.putString("token", signUpResponse.getString("token"));
                                mPreferenceManager.putString("sp_name", jsonObject.getString("name"));
                                mPreferenceManager.putString("is_delivery_staff", jsonObject.getString("is_delivery_staff"));

                                pushFcmToken();
                                EventBus.getDefault().post(new RegisterEvent(true, signUpResponse.getString("message")));
                            } else {
                                EventBus.getDefault().post(new RegisterEvent(signUpResponse.getBoolean("status"), 200, signUpResponse.getString("message")));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new RegisterEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void forgetPassword(JSONObject forgetPasswordJson) {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.FORGET_PASSWORD, forgetPasswordJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {
                        Log.d(TAG, loginResponse.toString());
                        try {

                            EventBus.getDefault().post(new ForgetPasswordEvent(true, loginResponse.getString("message"), loginResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        if (error.networkResponse.statusCode == 500) {
                            EventBus.getDefault().post(new ForgetPasswordEvent(false, error.networkResponse.statusCode, ""));
                        } else {
                            EventBus.getDefault().post(new ForgetPasswordEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getRegsiteredServicesList() {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.GET_REGISTER_SERVICES_LIST, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, spResponse.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<RegisterServicesModel>>() {
                        }.getType();
                        ArrayList<RegisterServicesModel> registerServicesModels = null;
                        try {
                            registerServicesModels = gson.fromJson(spResponse.getJSONArray("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new RegisteredServicesEvent(true, registerServicesModels));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new RegisteredServicesEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new RegisteredServicesEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //  Log.d("token: ", mPreferenceManager.getString("token"));
                //  params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void postServiceProviderReview(JSONObject reviewJson) {}/*{
        Log.d(TAG, "postServiceProviderReview: API " + reviewJson);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.CONSUMER_REVIEW, reviewJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject reviewResponse) {
                        Log.d(TAG, "postServiceProviderReview: RESPONSE " + reviewResponse.toString());
                        try {
                            EventBus.getDefault().post(new ConsumerReviewEvent(true, reviewResponse.getString("message"), reviewResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new ConsumerReviewEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void pushFcmToken() {}/*{
        JSONObject fcmJson = new JSONObject();

        try {
            SharedPreferences mSharedFcmPreferences = mContext.getSharedPreferences("FCM_PREF", 0);

            fcmJson.put("sp_id", mPreferenceManager.getString("sp_id"));
            fcmJson.put("fcm_token", mSharedFcmPreferences.getString("fcm_token", "NV"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.FCM_TOKEN_API, fcmJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject loginResponse) {
                        Log.d(TAG, loginResponse.toString());
                       *//* try {
                            JSONObject jsonObject = loginResponse.getJSONObject("data");

                            EventBus.getDefault().post(new LoginEvent(true, loginResponse.getString("message"), loginResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*//*
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new LoginEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new LoginEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getMessagesList(String consumerId) {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.GET_TRANSACTION_LIST + consumerId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject messagesResponse) {
                        try {
                            //  {"status":false,"data":[],"message":"No message"}

                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<MessagesModel>>() {
                            }.getType();
                            ArrayList<TransactionModel> modelList = gson.fromJson(messagesResponse.getJSONArray("data").toString(), type);
                            if (modelList.size() > 0) {
                                EventBus.getDefault().post(new TransactionListEvent(true, 200, modelList));
                            } else {
                                EventBus.getDefault().post(new TransactionListEvent(true, 200, messagesResponse.getString("message")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }


                if (errorJson != null && error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new TransactionListEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new TransactionListEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new MessagesListEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void sendFeedback(JSONObject loginJson) {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.SEND_FEEDBACK, loginJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject feedbackResponse) {
                        Log.d(TAG, feedbackResponse.toString());
                        try {

                            EventBus.getDefault().post(new FeedbackEvent(true, feedbackResponse.getString("message"), feedbackResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new FeedbackEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new FeedbackEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new LoginEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getFAQsList() {}/*{
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.GET_FAQ, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject messagesResponse) {
                        try {
                            //  {"status":false,"data":[],"message":"No message"}

                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<FAQModel>>() {
                            }.getType();
                            ArrayList<FAQModel> modelList = gson.fromJson(messagesResponse.getJSONArray("data").toString(), type);
                            if (modelList.size() > 0) {
                                EventBus.getDefault().post(new FAQListEvent(true, 200, modelList));
                            } else {
                                EventBus.getDefault().post(new FAQListEvent(true, 200, messagesResponse.getString("message")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }


                if (errorJson != null && error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new FAQListEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new FAQListEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new FAQListEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getUserTransactionList(String consumerId) {}/*{
        Log.d(TAG, "getUserTransactionList: " + Constants.BASE_URL + Constants.GET_TRANSACTION_LIST + consumerId);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.GET_TRANSACTION_LIST + consumerId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject messagesResponse) {
                        try {
                            //  {"status":false,"data":[],"message":"No message"}
                            Log.d(TAG, "getUserTransactionList: RESPONSE " + messagesResponse.toString());

                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<TransactionModel>>() {
                            }.getType();
                            ArrayList<TransactionModel> modelList = gson.fromJson(messagesResponse.getJSONArray("data").toString(), type);
                            if (modelList.size() > 0) {
                                EventBus.getDefault().post(new TransactionListEvent(true, 200, modelList));
                            } else {
                                EventBus.getDefault().post(new TransactionListEvent(true, 200, messagesResponse.getString("message")));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }


                if (errorJson != null && error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new TransactionListEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new TransactionListEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new TransactionListEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void changePassword(final JSONObject changepasswordJson) {}/*{
        Log.d(TAG, "CHANGE_PASSWORD: API " + Constants.BASE_URL + Constants.CHANGE_PASSWORD);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.CHANGE_PASSWORD, changepasswordJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "CHANGE_PASSWORD: RESPONSE : " + response.toString());
                        try {
                            EventBus.getDefault().post(new ChangePasswordEvent(true, 200, response.getString("message")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                //{"status":false,"data":[],"message":"You need to pass Your Mobile Number or Email id and Password!"}
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new ChangePasswordEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new ChangePasswordEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new ChangePasswordEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void editProfile(final JSONObject editProfileJson) {}/*{
        Log.d(TAG, "EDIT PROFILE: API " + Constants.BASE_URL + Constants.EDIT_PROFILE);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.EDIT_PROFILE, editProfileJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "EDIT PROFILE: RESPONSE : " + response.toString());
                        //{"status":true,"data":{"id":"149","name":"madhh","mobile":"9742094795","email":"madhu@infogsskaters.com "},"message":"Updated Successfully"}
                        try {
                            if (response.getBoolean("status")) {
                                mPreferenceManager.putString("mobile", response.getJSONObject("data").getString("mobile"));
                                mPreferenceManager.putString("email", response.getJSONObject("data").getString("email"));
                                mPreferenceManager.putString("sp_name", response.getJSONObject("data").getString("name"));
                            }
                            EventBus.getDefault().post(new EditProfileEvent(true, 200, response.getString("message")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                //{"status":false,"data":[],"message":"You need to pass Your Mobile Number or Email id and Password!"}
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (error.networkResponse != null) {
                    try {
                        if (errorJson != null)
                            EventBus.getDefault().post(new EditProfileEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        else
                            EventBus.getDefault().post(new EditProfileEvent(false, error.networkResponse.statusCode, ""));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new EditProfileEvent(false, 504, ""));
                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void sendSpLatLong() throws JSONException {}/*{
        JSONObject json = new JSONObject();
        json.put("sp_id", mPreferenceManager.getString("sp_id"));
        if (NServicesSingleton.getInstance().getMycurrentLatitude() != null) {
            json.put("latitude", NServicesSingleton.getInstance().getMycurrentLatitude());
        } else {
            //Utils.getLocationFromBackg(mContext);
            json.put("latitude", mPreferenceManager.getString("user_lat"));
        }

        if (NServicesSingleton.getInstance().getMycurrentLongitude() != null) {
            json.put("longitude", NServicesSingleton.getInstance().getMycurrentLongitude());
        } else {
            json.put("longitude", mPreferenceManager.getString("user_long"));
        }
        Log.d(TAG, "SEND LOCATION: API " + Constants.BASE_URL + Constants.UPDATE_LAT_LONG + " - " + json);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.UPDATE_LAT_LONG, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        //{"status":true,"data":[],"message":"Service provider status updates successfully"}
                        Log.d("SEND LOCATION RESPONSE:", spResponse.toString());

                        EventBus.getDefault().post(new TrackSPEvent(true, ""));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new TrackSPEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    EventBus.getDefault().post(new TrackSPEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //    Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void submitOtp(JSONObject otpJson) {}/*{
        Log.d(TAG, "submitOtp: API " + otpJson);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.VALIDATE_OTP, otpJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject otpResponse) {
                        Log.d(TAG, "submitOtp: RESPONSE " + otpResponse.toString());
                        try {
                            EventBus.getDefault().post(new ConsumerOtpEvent(otpResponse.getBoolean("status"), otpResponse.getString("message"), otpResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new ConsumerOtpEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getJobDetail(String bookingId, String spId) {}/*{

        Log.d(TAG, "getJobDetail: API " + Constants.BASE_URL + Constants.JOB_DETAIL + spId + "/" + bookingId);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.JOB_DETAIL + spId + "/" + bookingId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getJobDetail: RESPONSE " + spResponse.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<JobDetailModel>() {
                        }.getType();
                        JobDetailModel jobDetailModel = null;
                        try {
                            jobDetailModel = gson.fromJson(spResponse.getJSONObject("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new JobDetailEvent(true, jobDetailModel));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new JobDetailEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new TodaysJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    public void getSPDetails(String spId) {}/*{
        Log.d(TAG, "getJobDetail: API " + Constants.BASE_URL + Constants.SP_JOB_DETAIL + spId);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.SP_JOB_DETAIL + spId, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getSPJobDetail: RESPONSE " + spResponse.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<SPDetails>() {
                        }.getType();
                        SPDetails jobDetailModel = null;
                        try {
                            jobDetailModel = gson.fromJson(spResponse.getJSONObject("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new SPDetailEvent(true, "", jobDetailModel));

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new JobDetailEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    EventBus.getDefault().post(new TodaysJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //    params.put("Authorization", mPreferenceManager.getString("token"));
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

    /*public void getFoodJobDetail() {

        Log.d(TAG, "getJobDetail: API " + DiorderRequestFormRestaurants );
        JsonObjectRequest jsonRequest = new JsonObjectRequest(DiorderRequestFormRestaurants , null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject spResponse) {
                        Log.d(TAG, "getJobDetail: RESPONSE " + spResponse.toString());
                        Gson gson = new Gson();
                        *//*Type type = new TypeToken<FoodJobDetailModel>() {
                        }.getType();
                        FoodJobDetailModel jobDetailModel = null;
                        try {
                            jobDetailModel = gson.fromJson(spResponse.getJSONObject("data").toString(), type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        EventBus.getDefault().post(new FoodJobDetailEvent(true, jobDetailModel));*//*
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, "onErrorResponse: toString "+error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        //EventBus.getDefault().post(new FoodJobDetailEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                        errorJson.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //EventBus.getDefault().post(new TodaysJobEvent(false, 0, error.toString()));
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                //   Log.d("token: ", mPreferenceManager.getString("token"));
                //params.put("Authorization", mPreferenceManager.getString("token"));
                params.put("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzQwODQxOTN9.GrxNNgDY--eD3vLfdL0zhfMsFv4efNTWdKP4FXvrj6o");
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/


    public void getFoodJobDetail() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

            // Initialize a new JsonArrayRequest instance
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, DiorderRequestFormRestaurants, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, response.toString());

                    try {

                        Log.d(TAG, "getSPJobDetail: RESPONSE " + response.toString());
                        // Loop through the array elements
                        JSONObject jsonResult = new JSONObject(response.toString());
                        try {
                            String data = jsonResult.getString("data");
                            if (!data.equals("false")){
                                Log.d(TAG, "getSPJobDetail: RESPONSE  DATA " + data);

                                //Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();

                                notification();







                                /*NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext,"")
         .setSmallIcon(R.drawable.ic_home_black_24dp)
         .setContentTitle("Notifications Example")
         .setContentText("This is a test notification");

      Intent notificationIntent = new Intent(mContext, DashBoardActivity.class);
      PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent,
         PendingIntent.FLAG_UPDATE_CURRENT);
      builder.setContentIntent(contentIntent);*/

      // Add as notification
     /* NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
      manager.notify(0, builder.build());*/
                            }else {
                                //Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();
                               // notification();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            JSONArray districts = jsonResult.getJSONArray("data");

                            if (districts.length()>0) {
                                notification();
                            }
                            Log.d(TAG, "getSPJobDetail: RESPONSE  DATA ERROR" + e);
                        }
                       /* for (int i = 0; i < districts.length(); i++) {
                            // JSONArray data = response.getJSONArray("data");
                            JSONObject data1 = districts.getJSONObject(i);

                            String tempId = data1.getString("id");
                            String tempOrderId = data1.getString("order_id");
                            String tempOrderNo = data1.getString("order_no");
                            String tempRestaurent = data1.getString("resturant");
                            String tempRestaurentAddress = data1.getString("resturant_address");
                            String tempCustomer = data1.getString("customer");
                            String tempCustomerAddress = data1.getString("customer_address");


                            OrderListModelClass orderListModelClass = new OrderListModelClass();

                            orderListModelClass.setId(tempId);
                            orderListModelClass.setOrderId(tempOrderId);
                            orderListModelClass.setOrderNo(tempOrderNo);
                            orderListModelClass.setResturant(tempRestaurent);
                            orderListModelClass.setResturantAddress(tempRestaurentAddress);
                            orderListModelClass.setCustomer(tempCustomer);
                            orderListModelClass.setCustomerAddress(tempCustomerAddress);

                            orderListArrayList.add(orderListModelClass);

                        }*/

                        /*CurrentOrderListAdapter currentOrderListAdapter = new CurrentOrderListAdapter(orderListArrayList);
                        rvMyCurrentOrder.setAdapter(currentOrderListAdapter);*/


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: toString "+error.toString());
                    error.printStackTrace();
                    JSONObject errorJson = null;
                    //notification();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        //Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                        //UImsgs.showToast(mContext, getResources().getString(R.string.connection_time_out));
                    }

                    // As of f605da3 the following should work
                    NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            errorJson = new JSONObject(res);
                            Log.d(TAG, "onErrorResponse: " + errorJson);
                        } catch (UnsupportedEncodingException | JSONException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        }
                    }
                    if (errorJson != null && error.networkResponse != null) {
                        try {
                            //EventBus.getDefault().post(new FoodJobDetailEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                            errorJson.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        //EventBus.getDefault().post(new TodaysJobEvent(false, 0, error.toString()));
                    }
                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("X_AUTH_TOKEN", token);

                    return map;
                }
            };
       /* MyAdapter myAdapter=new MyAdapter(mContext,R.layout.list_view_array_adapter,time_Table);
        simpleList.setAdapter(myAdapter);*/
            requestQueue.add(jsonObjectRequest);
        }
    }

    public void notification(){
        int NOTIFICATION_ID = 234;


        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            String CHANNEL_ID = "my_channel_01";
            CharSequence name = "my_channel";
            String Description = "This is my channel";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            mChannel.setShowBadge(false);
            mChannel.setSound(null, null);
            notificationManager.createNotificationChannel(mChannel);
        }else if(android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.O){
           /* NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext,"")
         .setSmallIcon(R.mipmap.ic_launcher)
         .setContentTitle("Notifications Example")
         .setContentText("This is a test notification");

      Intent notificationIntent = new Intent(mContext, DashBoardActivity.class);
      PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, notificationIntent,
         PendingIntent.FLAG_UPDATE_CURRENT);
      builder.setContentIntent(contentIntent);

            // Add as notification
      NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
      manager.notify(0, builder.build());*/

        }
        String CHANNEL_ID = "my_channel_01";

        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("New Order Received")
                .setContentText("You Have A New Order To Deliver");
                //.setDefaults(0);
                //.setAutoCancel(true);

        Intent resultIntent = new Intent(mContext, DashBoardActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        stackBuilder.addParentStack(DashBoardActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        /*Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        builder.setSound(alarmSound);*/


       /* Notification notification = builder.build();
        notification.sound = Uri.parse("android.resource://"
                + mContext.getPackageName() + "/" + R.raw.alarm_beeping_sound);

        builder.setSound(notification.sound);*/

        //Media Player
        MediaPlayer mMediaPlayer = new MediaPlayer();
        mMediaPlayer = MediaPlayer.create(mContext, R.raw.alarm_beeping_sound);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setLooping(false);
        mMediaPlayer.start();

        notificationManager.notify(NOTIFICATION_ID, builder.build());

    };

    public void foodPaymentReceived(JSONObject foodPaymentJson) {}/*{
        Log.d(TAG, "foodPaymentReceived: API " + foodPaymentJson);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Constants.BASE_URL + Constants.SP_FOOD_PAYMENT_RECEIVED, foodPaymentJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject otpResponse) {
                        // {"status":true,"data":[],"message":"Payment successfully recived"}
                        Log.d(TAG, "foodPaymentReceived: RESPONSE " + otpResponse.toString());
                        try {
                            EventBus.getDefault().post(new FoodPayementRecievedEvent(otpResponse.getBoolean("status"), otpResponse.getString("message"), otpResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Log.d(TAG, error.toString());
                error.printStackTrace();
                JSONObject errorJson = null;
                // As of f605da3 the following should work
                NetworkResponse response = error.networkResponse;
                if (error instanceof ServerError && response != null) {
                    try {
                        String res = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                        // Now you can use any deserializer to make sense of data
                        errorJson = new JSONObject(res);
                        Log.d(TAG, "onErrorResponse: " + errorJson);
                        // Toast.makeText(mContext, errorJson.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedEncodingException | JSONException e1) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace();
                    }
                }
                if (errorJson != null && error.networkResponse != null) {
                    try {
                        EventBus.getDefault().post(new FoodPayementRecievedEvent(false, error.networkResponse.statusCode, errorJson.getString("message")));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        }) {
            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                // params.put("Authorization", getAuthHeader());
                return params;
            }
        };
        jsonRequest.setShouldCache(false);
        jsonRequest.setRetryPolicy(new DefaultRetryPolicy(10000, 2, 2));
        CustomerServicesApp.getInstance().addToRequestQueue(jsonRequest);
    }*/

}

