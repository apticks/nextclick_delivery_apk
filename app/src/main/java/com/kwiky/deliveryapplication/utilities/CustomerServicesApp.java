package com.kwiky.deliveryapplication.utilities;

import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.android.volley.ExecutorDelivery;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NoCache;
//import com.crashlytics.android.Crashlytics;

import java.util.concurrent.ThreadPoolExecutor;

//import io.fabric.sdk.android.Fabric;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by Chanchal Shakti on 15/11/'19.
 */

public class CustomerServicesApp extends Application {
    private static CustomerServicesApp customerServicesAppInstance;
    RequestQueue mRequestQueue;
    private static final String DEFAULT_CACHE_DIR = "volley";
    private static final int DEFAULT_NETWORK_THREAD_POOL_SIZE = 5;
    private ImageLoader mImageLoader;
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "fr"));
    }
    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        customerServicesAppInstance = this;
    }

    public static synchronized CustomerServicesApp getInstance() {
        return customerServicesAppInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            java.io.File cacheDir = new java.io.File(getCacheDir(), DEFAULT_CACHE_DIR);
            Network network = new BasicNetwork(new HurlStack());
            ThreadPoolExecutor tp = (ThreadPoolExecutor) AsyncTask.THREAD_POOL_EXECUTOR;
            tp.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardOldestPolicy());
            ExecutorDelivery exec = new ExecutorDelivery(tp);
            mRequestQueue = new RequestQueue(new NoCache(), network,
                    DEFAULT_NETWORK_THREAD_POOL_SIZE, exec);
            mRequestQueue.getCache().clear();
            mRequestQueue.start();
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {

        req.setShouldCache(false);
        getRequestQueue().getCache().clear();
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            /*mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());*/
        }
        return this.mImageLoader;
    }
}
