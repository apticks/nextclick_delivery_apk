package com.kwiky.deliveryapplication.ui.currentOrders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kwiky.deliveryapplication.ActivitiesActivity.DashBoardActivity;
import com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity;
import com.kwiky.deliveryapplication.ActivitiesActivity.SplashActivity;
import com.kwiky.deliveryapplication.Adapters.CurrentOrderListAdapter;
import com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs;
import com.kwiky.deliveryapplication.PojoClass.OrderListModelClass;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;
import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.DiorderRequestFormRestaurants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CurrentOrderList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CurrentOrderList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurrentOrderList extends Fragment {
    Context mContext;
    //View
    View view;
    //Recycle View
    public RecyclerView rvMyCurrentOrder;
    //Swipe REfresh
    SwipeRefreshLayout refresh_swipe;
    List<OrderListModelClass> orderListArrayList;
    public LinearLayout frame_layout_empty;
    private CountDownTimer timer;
    public static String token;

    PreferenceManager preferenceManager;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CurrentOrderList() {
        // Required empty public constructor
    }

    public CurrentOrderList(Context mContext) {
        // Required empty public constructor
        this.mContext = mContext;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CurrentOrderList.
     */
    // TODO: Rename and change types and number of parameters
    public static CurrentOrderList newInstance(String param1, String param2) {
        CurrentOrderList fragment = new CurrentOrderList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_current_order_list, container, false);
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        // Inflate the layout for this fragment
        initView();

        //currentOrderAPI()
        currentOrderAPI();

        //Swipe refresh
        swipeRefresh();


       // new Repeater().execute();

        return view;
    }




    private void swipeRefresh() {
        refresh_swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Clear List

                orderListArrayList.clear();
                currentOrderAPI();
                //Calling Current Order API
                refresh_swipe.setRefreshing(false);
            }
        });
    }

    public void currentOrderAPI() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        // Initialize a new JsonArrayRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, DiorderRequestFormRestaurants, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResturentOrder", "" + response);
                try {
                    // Loop through the array elements

                    JSONObject jsonResult = new JSONObject(response.toString());
                    JSONArray districts = jsonResult.getJSONArray("data");
                    orderListArrayList = new ArrayList<>();
                    for (int i = 0; i < districts.length(); i++) {
                        // JSONArray data = response.getJSONArray("data");
                        JSONObject data1 = districts.getJSONObject(i);

                        String tempId = data1.getString("id");
                        String tempOrderId = data1.getString("order_id");
                        String tempOrderNo = data1.getString("order_no");
                        String tempRestaurent = data1.getString("resturant");
                        String tempRestaurentAddress = data1.getString("resturant_address");
                        String tempCustomer = data1.getString("customer");
                        String tempCustomerAddress = data1.getString("customer_address");
                        String Created_at = data1.getString("created_at");

                        OrderListModelClass orderListModelClass = new OrderListModelClass();

                        orderListModelClass.setId(tempId);
                        orderListModelClass.setOrderId(tempOrderId);
                        orderListModelClass.setOrderNo(tempOrderNo);
                        orderListModelClass.setResturant(tempRestaurent);
                        orderListModelClass.setResturantAddress(tempRestaurentAddress);
                        orderListModelClass.setCustomer(tempCustomer);
                        orderListModelClass.setCustomerAddress(tempCustomerAddress);
                        orderListModelClass.setCreated_at(Created_at);
                        orderListArrayList.add(orderListModelClass);
                        frame_layout_empty.setVisibility(View.GONE);
                    }
                    CurrentOrderListAdapter currentOrderListAdapter = new CurrentOrderListAdapter(orderListArrayList);
                    rvMyCurrentOrder.setAdapter(currentOrderListAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    frame_layout_empty.setVisibility(View.VISIBLE);
                    frame_layout_empty.setGravity(Gravity.CENTER);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                   // UImsgs.showToast(mContext, getResources().getString(R.string.connection_time_out));
                }
            }
        }) {
                /*@Override
                public String getBodyContentType() {
                    return "application/json";
                }*/

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);
                return map;
            }
        };
//        MyAdapter myAdapter=new MyAdapter(this,R.layout.list_view_array_adapter,time_Table);
//        simpleList.setAdapter(myAdapter);
        requestQueue.add(jsonObjectRequest);
    }




    /*public void currentOrderAPI(final Context context) {

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        // Initialize a new JsonArrayRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, DiorderRequestFormRestaurants, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResturentOrder", "" + response);
                try {
                    // Loop through the array elements

                    JSONObject jsonResult = new JSONObject(response.toString());
                    JSONArray districts = jsonResult.getJSONArray("data");
                    for (int i = 0; i < districts.length(); i++) {
                        // JSONArray data = response.getJSONArray("data");
                        JSONObject data1 = districts.getJSONObject(i);

                        String tempId = data1.getString("id");
                        String tempOrderId = data1.getString("order_id");
                        String tempOrderNo = data1.getString("order_no");
                        String tempRestaurent = data1.getString("resturant");
                        String tempRestaurentAddress = data1.getString("resturant_address");
                        String tempCustomer = data1.getString("customer");
                        String tempCustomerAddress = data1.getString("customer_address");
                        String Created_at = data1.getString("created_at");

                        OrderListModelClass orderListModelClass = new OrderListModelClass();

                        orderListModelClass.setId(tempId);
                        orderListModelClass.setOrderId(tempOrderId);
                        orderListModelClass.setOrderNo(tempOrderNo);
                        orderListModelClass.setResturant(tempRestaurent);
                        orderListModelClass.setResturantAddress(tempRestaurentAddress);
                        orderListModelClass.setCustomer(tempCustomer);
                        orderListModelClass.setCustomerAddress(tempCustomerAddress);
                        orderListModelClass.setCreated_at(Created_at);
                        orderListArrayList.add(orderListModelClass);
//                        frame_layout_empty.setVisibility(View.GONE);
                    }
                    CurrentOrderListAdapter currentOrderListAdapter = new CurrentOrderListAdapter(orderListArrayList);
//                    rvMyCurrentOrder.setAdapter(currentOrderListAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
 //                   frame_layout_empty.setVisibility(View.VISIBLE);
   //                 frame_layout_empty.setGravity(Gravity.CENTER);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    UImsgs.showToast(context, getResources().getString(R.string.connection_time_out));
                }
            }
        }) {
                *//*@Override
                public String getBodyContentType() {
                    return "application/json";
                }*//*

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1ODMxNTIxNDR9.kMzk-ekyVcV3M4A2tvKjQOBivz1cemz2EgR2uMgrGe8");
                return map;
            }
        };
//        MyAdapter myAdapter=new MyAdapter(this,R.layout.list_view_array_adapter,time_Table);
//        simpleList.setAdapter(myAdapter);
        requestQueue.add(jsonObjectRequest);
    }*/


    //

    /*@Override
                public String getBodyContentType() {
                    return "application/json";
                }*//*

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1ODMxNTIxNDR9.kMzk-ekyVcV3M4A2tvKjQOBivz1cemz2EgR2uMgrGe8");
                return map;
            }
        };
//        MyAdapter myAdapter=new MyAdapter(this,R.layout.list_view_array_adapter,time_Table);
//        simpleList.setAdapter(myAdapter);
        requestQueue.add(jsonObjectRequest);
    }*/

    //
        /*{
            String tag_json_obj = "json_socity_req";
            Map<String, String> params = new HashMap<String, String>();
            params.put("d_id", get_id);

            CustomVolleyJsonArrayRequest jsonObjReq = new CustomVolleyJsonArrayRequest(Request.Method.POST,
                    BaseURL.GET_DELIVERD_ORDER_URL, params, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<MyPastOrderModel>>() {
                    }.getType();
                    my_order_modelList = gson.fromJson(response.toString(), listType);
                    OnGoingOrderAdapter adapter = new OnGoingOrderAdapter(my_order_modelList);
                    rv_myorder.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    if (my_order_modelList.isEmpty()) {
                        // Toast.makeText(getActivity(), getResources().getString(R.string.no_rcord_found), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


        }*/


    private void initView() {
        rvMyCurrentOrder = view.findViewById(R.id.rv_mycurrentorder);

        refresh_swipe = view.findViewById(R.id.refresh_swipe);
        refresh_swipe.setColorSchemeResources(R.color.Green);

        // Linear Layout Frame
        frame_layout_empty = view.findViewById(R.id.frame_layout_empty);


        RecyclerView.LayoutManager mlayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvMyCurrentOrder.setHasFixedSize(false);
        rvMyCurrentOrder.setLayoutManager(mlayoutmanager);
        rvMyCurrentOrder.setItemViewCacheSize(10);
        rvMyCurrentOrder.setDrawingCacheEnabled(true);
        rvMyCurrentOrder.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    /*@Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        startTimer(60000);
    }

    private void startTimer(int duration){
        timer= new CountDownTimer(duration, 5000) {

            public void onTick(long millisUntilFinished) {
                currentOrderAPI();
            }

            public void onFinish() {
                startTimer(60000);
            }
        }.start();
    }




    public class Repeater extends AsyncTask<String, String, String >{

        @Override
        protected String doInBackground(String... strings) {
            currentOrderAPI();
            return null;
        }
    }
}
