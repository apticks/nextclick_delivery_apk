package com.kwiky.deliveryapplication.ui.MapTrack;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
