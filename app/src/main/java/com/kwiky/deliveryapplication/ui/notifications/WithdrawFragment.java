package com.kwiky.deliveryapplication.ui.notifications;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;

public class WithdrawFragment extends Fragment {

    private ProgressDialog pDialog;
    private ArrayList<HashMap<String, String>> offersList;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERID = "userid";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_MOBILE = "mobile";
    private static final String TAG_USERBALANCE = "balance";
    JSONArray jsonarray = null;
    private static PreferenceManager prf;
    private int success;
    private Context context;
    private String AmountToWithdraw;
    private String availableBalance;
    private TextView errorMessage;
    private String paytmNo, upi_id_str, bank_name_str, bank_person_name_str, ifsc_code_str, ac_no_str;
    private TextInputEditText paytmNumber, upi_id, bank_name, bank_person_name, ifsc_code, ac_no;
    private String username;
    private Button withdraw;
    private TextInputEditText withdrawAmount;
    private int withdrawalAmount;
    private RadioButton paytm, upi, bank;
    private RadioGroup transaction_type_group;
    private LinearLayout fields_layout, paytm_layout, upi_layout, bank_layout;
    SharedPreferences sharedPreferences;
    String storedUserToken;
    String withdraworder_id;
    String bal;
    View rootViewone;
    final int min = 1000;
    final int max = 10000;

    String token;

    PreferenceManager preferenceManager;

    public WithdrawFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        offersList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootViewone = inflater.inflate(R.layout.fragment_withdraw, container, false);
        sharedPreferences = getActivity().getSharedPreferences("Deliverysharedpreferences", Context.MODE_PRIVATE);
        storedUserToken = sharedPreferences.getString("user_token", null);

        preferenceManager = new PreferenceManager(getActivity());
        token = preferenceManager.getString(TOKEN_KEY);

        final int random = new Random().nextInt((max - min) + 1) + min;
        withdraworder_id = sharedPreferences.getString("unique_id", null) + "-" + random;

        initView(rootViewone);

        /*username = prf.getString(TAG_USERNAME);
        paytmNo = sharedPreferences.getString(TAG_MOBILE,null);
        availableBalance = prf.getString(TAG_USERBALANCE);*/

        paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fields_layout.setVisibility(View.VISIBLE);
                paytm_layout.setVisibility(View.VISIBLE);
                upi_layout.setVisibility(View.GONE);
                bank_layout.setVisibility(View.GONE);
            }
        });
        bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fields_layout.setVisibility(View.VISIBLE);
                paytm_layout.setVisibility(View.GONE);
                upi_layout.setVisibility(View.GONE);
                bank_layout.setVisibility(View.VISIBLE);
            }
        });
        upi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fields_layout.setVisibility(View.VISIBLE);
                paytm_layout.setVisibility(View.GONE);
                upi_layout.setVisibility(View.VISIBLE);
                bank_layout.setVisibility(View.GONE);
            }
        });
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AmountToWithdraw = withdrawAmount.getText().toString().trim();
                paytmNo = paytmNumber.getText().toString();
                upi_id_str = upi_id.getText().toString().trim();
                bank_name_str = bank_name.getText().toString().trim();
                bank_person_name_str = bank_person_name.getText().toString().trim();
                ifsc_code_str = ifsc_code.getText().toString().trim();
                ac_no_str = ac_no.getText().toString().trim();

                if (paytm.isChecked()) {
                    paytmNumber.setText(paytmNo);
                } else if (bank.isChecked()) {

                } else if (upi.isChecked()) {

                }
                if (!AmountToWithdraw.isEmpty()) {
                    withdrawalAmount = Integer.parseInt(AmountToWithdraw);
                    if ((withdrawalAmount > Integer.parseInt(sharedPreferences.getString("wallet", "0" /*null*/)))) {
                        Toast.makeText(context, "Unable to withdraw due to low balance", Toast.LENGTH_LONG).show();
                    } else {
                        if ((withdrawalAmount >= Integer.parseInt(sharedPreferences.getString("vendor_validation", null)))) {
                            Map<String, String> mapToParse = new HashMap<>();
                            mapToParse.put("ORDERID", withdraworder_id);
                            mapToParse.put("TXNAMOUNT", AmountToWithdraw);
                            mapToParse.put("DESC", " ");
                            mapToParse.put("PAYTM", paytmNo);
                            mapToParse.put("UPI", upi_id_str);
                            mapToParse.put("NAME", bank_person_name_str);
                            mapToParse.put("BANK_NAME", bank_name_str);
                            mapToParse.put("IFSC", ifsc_code_str);
                            mapToParse.put("AC", ac_no_str);

                            JSONObject jsonObject = new JSONObject(mapToParse);

                            submitWithdrawData(jsonObject, AmountToWithdraw);
                            return;
                        } else {
                            Toast.makeText(context, "You haven't reached the minimum withdraw count", Toast.LENGTH_LONG).show();
                        }
                    }
                }
               /* errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText("Enter Withdrawal Amount");
                errorMessage.setTextColor(Color.parseColor("#ff0000"));*/
            }
        });

        return rootViewone;
    }

    protected void initView(View view) {
        paytmNumber = (TextInputEditText) view.findViewById(R.id.numberWithdraw);
        upi_id = (TextInputEditText) view.findViewById(R.id.upi_id);
        bank_name = (TextInputEditText) view.findViewById(R.id.bank_name);
        bank_person_name = (TextInputEditText) view.findViewById(R.id.bank_person_name_id);
        ifsc_code = (TextInputEditText) view.findViewById(R.id.ifsc_id);
        ac_no = (TextInputEditText) view.findViewById(R.id.ac_no_id);

        withdrawAmount = (TextInputEditText) view.findViewById(R.id.amountWithdraw);
        withdraw = (Button) view.findViewById(R.id.withdrawButton);
        errorMessage = (TextView) view.findViewById(R.id.errorMsg);
        paytm = (RadioButton) view.findViewById(R.id.paytm_radio);
        bank = (RadioButton) view.findViewById(R.id.bank_radio);
        upi = (RadioButton) view.findViewById(R.id.upi_radio);

        fields_layout = (LinearLayout) view.findViewById(R.id.fields_layout);
        paytm_layout = (LinearLayout) view.findViewById(R.id.paytm_layout);
        upi_layout = (LinearLayout) view.findViewById(R.id.upi_layout);
        bank_layout = (LinearLayout) view.findViewById(R.id.bank_layout);

        transaction_type_group = view.findViewById(R.id.transaction_group);
    }

    private void submitWithdrawData(JSONObject jsonObject, String amount) {
        withDraw(jsonObject, amount);
    }

    private boolean validateWithdrawalAmount() {
        if (withdrawalAmount > Integer.parseInt(availableBalance)) {
            errorMessage.setText("withdrawal amount is greater than Available balance");
            errorMessage.setTextColor(Color.parseColor("#ff0000"));
            errorMessage.setVisibility(View.VISIBLE);
            return false;
        }
        if (withdrawalAmount >= 50) {
            return true;
        }
        errorMessage.setText("Minimum withdrawal amount is ₹ 50.");
        errorMessage.setTextColor(Color.parseColor("#ff0000"));
        errorMessage.setVisibility(View.VISIBLE);
        return false;
    }

    private boolean validatePaytmNumber() {
        if (paytmNo.length() <= 10) {
            if (paytmNo.length() >= 10) {
                return true;
            }
        }
        paytmNumber.setError("Paytm Number should be of 10 Digits");
        return false;
    }

    private void withDraw(JSONObject json, final String amount) {
        final String data = json.toString();
        Log.e("json", data);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://cineplant.com/nextclick/payment/api/payment/wallet/withdrawal", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status_code = jsonObject.getString("status_code");
                    if (status_code.equalsIgnoreCase("200")) {
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("wallet", (Integer.parseInt(sharedPreferences.getString("wallet", null)) - Integer.parseInt(amount)) + "".trim());
                        editor.commit();
                        bal = sharedPreferences.getString("wallet", null);
                    }
//                    ((WalletFragment)getActivity()).upadater(bal);
                    FragmentManager fragmentManager = getFragmentManager();
                    WalletFragment withdrawFragment = (WalletFragment) fragmentManager.findFragmentById(R.id.transactions_frag);
                    withdrawFragment.upadater(bal);/*transactions()*/
                    ;

                    /*TransactionsFragment.getInstance().transactions();
                    TransactionsFragment.getInstance().transactions();
                    TransactionsFragment.getInstance().transactions();*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error + "", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN",token);
                return map;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
