package com.kwiky.deliveryapplication.ui.notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Config;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.kwiky.deliveryapplication.Adapters.TransactionsAdapter;
import com.kwiky.deliveryapplication.Helpers.RecyclerTouchListener;
import com.kwiky.deliveryapplication.PojoClass.Transactions;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;

public class TransactionsFragment extends Fragment {
    private ArrayList<Map<String, String>> offersList;

    // url to get all products list
    private static TransactionsFragment instance = null;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RAJANR = "rajanr";

    //user
    private static final String TAG_USERID = "userid";
    private static final String TAG_USERNAME = "username";

    //usertransactions
    private static final String TAG_TRNID = "trnid";
    private static final String TAG_AMOUNT = "amount";
    private static final String TAG_TRNTYPE = "type";
    private static final String TAG_PAYTMNUMBER = "paytmnumber";
    private static final String TAG_TRNSTATUS = "status";
    private static final String TAG_TXNDesc = "txn_desc";

    //match
    private static final String TAG_LOG_ENTDATE = "log_entdate";
    private View rootViewone;

    // products JSONArray
    private JSONArray jsonarray = null;

    //Prefrance
    private static PreferenceManager prf;

    private int success;

    //new
    private Context context;

    private List<Transactions> transactionsList;
    private RecyclerView recyclerView;


    private ShimmerFrameLayout mShimmerViewContainer;
    private LinearLayout noTxnsLayout;
    private String username;

    private SwipeRefreshLayout transactionsRefresh;

    SharedPreferences sharedPreferences;
    String storedUserToken;


    String token;

    PreferenceManager preferenceManager;

    public TransactionsFragment() {
        // Required empty public constructor
    }

    public static TransactionsFragment getInstance() {
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getActivity();


        // Hashmap for ListView
       /* offersList = new ArrayList<>();

        new OneLoadAllProducts().execute();*/
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootViewone = inflater.inflate(R.layout.fragment_transaction, container, false);
        instance = this;
        sharedPreferences = getActivity().getSharedPreferences("Deliverysharedpreferences", Context.MODE_PRIVATE);
        storedUserToken = sharedPreferences.getString("user_token", null);

        preferenceManager = new PreferenceManager(getActivity());
        token = preferenceManager.getString(TOKEN_KEY);

        mShimmerViewContainer = (ShimmerFrameLayout) rootViewone.findViewById(R.id.shimmer_container);
        transactionsRefresh = (SwipeRefreshLayout) rootViewone.findViewById(R.id.transaction_refresher);
        noTxnsLayout = (LinearLayout) rootViewone.findViewById(R.id.noTxnLayout);

        mShimmerViewContainer.setVisibility(View.VISIBLE);


        username = sharedPreferences.getString(TAG_USERNAME, null);


        recyclerView = (RecyclerView) rootViewone.findViewById(R.id.txnListRecyclerView);

        transactionsRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                transactions();
                transactionsRefresh.setRefreshing(false);
            }
        });



        /*recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep transactions_list_rowist_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);

        // horizontal RecyclerView
        // keep transactions_list_rowist_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listenerMyDividerItemDecoration
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener_() {
            @Override
            public void onClick(View view, int position) {
                Transactions transactions = transactionsList.get(position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/
        transactions();
        return rootViewone;
    }

    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();

    }

    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();

    }

    /**
     * Prepares sample data to provide data set to adapter
     */

    /* class OneLoadAllProducts extends AsyncTask<String, String, String> {*/

    /**
     * getting All products from url
     * <p>
     * t
     */


    /*@RequiresApi(api = Build.VERSION_CODES.KITKAT)*/
    public void transactions() {
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://cineplant.com/nextclick/payment/api/payment/wallet/history", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    boolean status = jsonObject.getBoolean("status");
                    int http_code = jsonObject.getInt("http_code");
                    String message = jsonObject.getString("message");
                    if (status & http_code == 200 & message.equalsIgnoreCase("Success..!")) {
                        JSONArray data = jsonObject.getJSONArray("data");

                        if (data.length() > 0) {
                            transactionsList = new ArrayList<>();
                            offersList = new ArrayList<>();
                            for (int i = 0; i < data.length(); i++) {

                                Map<String, String> txn_map = new HashMap<>();

                                JSONObject dataObject = data.getJSONObject(i);
                                String amount = dataObject.getString("cash");
                                String txn_id = dataObject.getString("txn_id");
                                String end_date = dataObject.getString("created_at");
                                int txn_status = dataObject.getInt("status");
                                String txn_status_str = "";
                                if (txn_status == 0) {
                                    txn_status_str = "pending";
                                } else if (txn_status == 1) {
                                    txn_status_str = "success";
                                } else if (txn_status == 2) {
                                    txn_status_str = "failed";
                                }

                                String type = dataObject.getString("type");
                                String desc = dataObject.getString("description");

                                txn_map.put(TAG_AMOUNT, amount);
                                txn_map.put(TAG_LOG_ENTDATE, end_date.substring(0, 10));
                                txn_map.put(TAG_TRNSTATUS, txn_status_str);
                                txn_map.put(TAG_TRNTYPE, type);
                                txn_map.put(TAG_TXNDesc, desc);

                                offersList.add(txn_map);


                            }
                            for (int i = 0; i < offersList.size(); i++) {

                                //System.out.println("Rjn_userid"+offersList.get(i).get(TAG_USERID));
                                Transactions transactions = new Transactions();
                                transactions.setTxnAmount(offersList.get(i).get(TAG_AMOUNT));
                                transactions.setTxnDate(offersList.get(i).get(TAG_LOG_ENTDATE));
                                transactions.setTxnRemark(offersList.get(i).get(TAG_TRNSTATUS));
                                transactions.setTxnType(offersList.get(i).get(TAG_TRNTYPE));
                                transactions.setTxnDesc(offersList.get(i).get(TAG_TXNDesc));

                                transactionsList.add(transactions);

                                // notify adapter about data set changes
                                // so that it will render the list with new data

                            }

                            if (transactionsList.size() <= 0) {
                                mShimmerViewContainer.stopShimmer();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                noTxnsLayout.setVisibility(View.VISIBLE);
                                return;
                            }

                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            noTxnsLayout.setVisibility(View.GONE);
                        }
                        TransactionsAdapter mAdapter = new TransactionsAdapter(transactionsList);

                        recyclerView.setHasFixedSize(true);

                        // vertical RecyclerView
                        // keep transactions_list_rowist_row.xml width to `match_parent`
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);

                        // horizontal RecyclerView
                        // keep transactions_list_rowist_row.xml width to `wrap_content`
                        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

                        recyclerView.setLayoutManager(mLayoutManager);

                        // adding inbuilt divider line
                        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.VERTICAL));

                        // adding custom divider line with padding 16dp
                        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());

                        recyclerView.setAdapter(mAdapter);

                        // row click listenerMyDividerItemDecoration
                        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context, recyclerView, new RecyclerTouchListener.ClickListener_() {
                            @Override
                            public void onClick(View view, int position) {
                                Transactions transactions = transactionsList.get(position);
                            }

                            @Override
                            public void onLongClick(View view, int position) {

                            }
                        }));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //ps_exec();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, error + "", Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("Content-Type", "application/json");
                map.put("X_AUTH_TOKEN", token);

                return map;
            }

        };
        requestQueue.add(stringRequest);


    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
       /* protected void ps_exec() {

            // updating UI from Background Thread

                    *//*
                      Updating parsed JSON data into ListView
                     *//*

     *//* for (int i = 0; i < offersList.size(); i++) {

                            //System.out.println("Rjn_userid"+offersList.get(i).get(TAG_USERID));
                            Transactions transactions = new Transactions();
                            transactions.setTxnAmount(offersList.get(i).get(TAG_AMOUNT));
                            transactions.setTxnDate(offersList.get(i).get(TAG_LOG_ENTDATE));
                            transactions.setTxnRemark(offersList.get(i).get(TAG_TRNSTATUS));
                            transactions.setTxnType(offersList.get(i).get(TAG_TRNTYPE));

                            transactionsList.add(transactions);

                            // notify adapter about data set changes
                            // so that it will render the list with new data
                            mAdapter.notifyDataSetChanged();

                        }

                        if (transactionsList.size() <= 0) {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            noTxnsLayout.setVisibility(View.VISIBLE);
                            return;
                        }

                        System.out.println("Rjn_mShimmerViewContainer.stopShimmer()");
                        mShimmerViewContainer.stopShimmer();
                        mShimmerViewContainer.setVisibility(View.GONE);
                        noTxnsLayout.setVisibility(View.GONE);
*//*
     *//* else if(success == 2){
                        // no jsonarray found
                        System.out.println("Rjn_mobile_already_exist");
//                        Toast.makeText(context,"Userid is not valid",Toast.LENGTH_LONG).show();

                    } else {
                        System.out.println("Rjn_user_not_created");
                        Toast.makeText(context,"Something went wrong. Try again!", Toast.LENGTH_LONG).show();

                    }*//*



        }*/

}


