package com.kwiky.deliveryapplication.ui.notifications;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity;
import com.kwiky.deliveryapplication.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.kwiky.deliveryapplication.constants.Urls.PROFILE;
import static com.kwiky.deliveryapplication.constants.Urls.PROFILE_PIC_R;

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private View rootView;
    private CardView customerSupport, aboutUsCard, howtoJoinCard, privacypolicyCard, logOutCard, idCard;
    private Context mContext;
    TextView text_name, text_my_unique_id, text_myBalance;
    private SharedPreferences sharedPreferences;
    String Unique_id, name, Wallet, PEmail;
    TextView prof_name, prof_ID, prof_mail;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        mContext = getActivity();
        text_name = (TextView) rootView.findViewById(R.id.text_name);
        text_my_unique_id = (TextView) rootView.findViewById(R.id.text_my_unique_id);
        text_myBalance = (TextView) rootView.findViewById(R.id.text_myBalance);
        sharedPreferences = mContext.getSharedPreferences("Delivery", Context.MODE_PRIVATE);
        FindIds();
//        profile_picRetrieval();
        setProfileValue();
        return rootView;
    }

    // create Method For Find ids from Xml
    private void FindIds() {
        logOutCard = (CardView) rootView.findViewById(R.id.logOutCard);
        aboutUsCard = (CardView) rootView.findViewById(R.id.aboutUsCard);
        privacypolicyCard = (CardView) rootView.findViewById(R.id.privacypolicyCard);
        howtoJoinCard = (CardView) rootView.findViewById(R.id.howtoJoinCard);
        customerSupport = (CardView) rootView.findViewById(R.id.customerSupportCard);
        idCard = (CardView) rootView.findViewById(R.id.idCard);


        // here click all cardsView
        customerSupport.setOnClickListener(this);
        aboutUsCard.setOnClickListener(this);
        howtoJoinCard.setOnClickListener(this);
        privacypolicyCard.setOnClickListener(this);
        logOutCard.setOnClickListener(this);
        idCard.setOnClickListener(this);
    }

    // create onClick method For perform action to Cardview
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.customerSupportCard:
                openWhatsApp();
                break;

            case R.id.aboutUsCard:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://nextclick.in")));
                break;

            case R.id.howtoJoinCard:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://nextclick.in/exe")));
                break;

            case R.id.privacypolicyCard:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.nextclick.in")));
                break;

            case R.id.logOutCard:
                startActivity(new Intent(getActivity(), LoginActivity.class));
                break;

            case R.id.idCard:
                openDialogBox();
                break;

        }
    }

    // Connect for whatsapp
    private void openWhatsApp() {
        try {
            String text = "";
            String toNumber = "919390755642";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + toNumber + "&text=" + text));
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* public void profileFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", "" + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Boolean status = jsonObject.getBoolean("status");
                    String http = jsonObject.getString("http_code");
                    String message = jsonObject.getString("message");

                    if (status && http.equalsIgnoreCase("200")
                            && message.equalsIgnoreCase("Success..!")) {

                        JSONObject data = jsonObject.getJSONObject("data");
                        uniqueID = data.getString("unique_id");
                        name = data.getString("first_name");
                        email = data.getString("email");
                        mobile = data.getString("phone");

                        Log.e("Data", "\n" + uniqueID + "\n" + name + "\n" + email + "\n" + mobile);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("unique_id", uniqueID);
                        editor.commit();

                        LayoutInflater inflater = getLayoutInflater();
                        View alertLayout = inflater.inflate(R.layout.id_layout, null);
                        name_tv = alertLayout.findViewById(R.id.prof_name);
                        mobile_tv = alertLayout.findViewById(R.id.prof_mobile);
                        mail_tv = alertLayout.findViewById(R.id.prof_mail);
                        id_tv = alertLayout.findViewById(R.id.prof_ID);
                        profile_pic = alertLayout.findViewById(R.id.profile_pic);

                        Log.d("image", image_url);
                        //profile_picRetrieval(profile_pic_id);
                        name_tv.setText(name);
                        mobile_tv.setText(mobile);
                        mail_tv.setText(email);
                        id_tv.setText(uniqueID);
                        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

                        alert.setView(alertLayout);
                        alert.setCancelable(true);

                        AlertDialog dialog = alert.create();
                        dialog.show();
                    }
                    Picasso.get().load(image_url)
                            .networkPolicy(NetworkPolicy.NO_CACHE)
                            .memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(profile_pic);
                } catch (Exception e) {
                    Toast.makeText(mContext, e + "", Toast.LENGTH_SHORT).show();
                    Log.d("error", e + "");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mContext, error + "", Toast.LENGTH_SHORT).show();
            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);
                // map.put("X_AUTH_TOKEN", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEiLCJ1c2VyZGV0YWlsIjp7InVzZXJuYW1lIjoiYWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AYWRtaW4uY29tIiwicGhvbmUiOiIwIn0sInRpbWUiOjE1NzQwODQxOTN9.GrxNNgDY--eD3vLfdL0zhfMsFv4efNTWdKP4FXvrj6o");

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }*/

    private void openDialogBox() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.id_layout, null);

        prof_name = (TextView) alertLayout.findViewById(R.id.prof_name);
        prof_ID = (TextView) alertLayout.findViewById(R.id.prof_ID);
        prof_mail = (TextView) alertLayout.findViewById(R.id.prof_mail);

        prof_name.setText(name);
        prof_ID.setText(Unique_id);
        prof_mail.setText(PEmail);
        AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

        alert.setView(alertLayout);
        alert.setCancelable(true);
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void setProfileValue() {
        Unique_id = sharedPreferences.getString("unique_id", null);
        name = sharedPreferences.getString("first_name", null);
        Wallet = sharedPreferences.getString("wallet", null);
        PEmail = sharedPreferences.getString("email", null);

        text_name.setText(name);
        text_myBalance.setText(Wallet);
        text_my_unique_id.setText(Unique_id);

    }
}