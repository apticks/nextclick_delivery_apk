package com.kwiky.deliveryapplication.ui.currentOrders;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.*;
/*import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;*/
import com.android.volley.toolbox.*;
/*import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;*/
import com.kwiky.deliveryapplication.Adapters.OnGoingOrderAdapter;
import com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs;
import com.kwiky.deliveryapplication.PojoClass.MyPastOrderModel;
import com.kwiky.deliveryapplication.PojoClass.OrderListModelClass;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;
import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.*;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnGoingOrderList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OnGoingOrderList#newInstance} factory method to
 * create an instance of this fragment.
 */

public class OnGoingOrderList extends Fragment {

    private RecyclerView rv_myorder;
    SwipeRefreshLayout refresh_swipe_ongoing;
    private List<MyPastOrderModel> my_order_modelList;
    View view;
    Context mContext;
    List<OrderListModelClass> acceptedOrderListArrayList = new ArrayList();
    LinearLayout frame_layout_empty;
    private CountDownTimer timer;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String token;

    PreferenceManager preferenceManager;

    private OnFragmentInteractionListener mListener;

    public OnGoingOrderList() {
        // Required empty public constructor
    }

    public OnGoingOrderList(Context mContext) {
        // Required empty public constructor
        //Context
        //initView();
        this.mContext = mContext;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OnGoingOrderList.
     */

    // TODO: Rename and change types and number of parameters
    public static OnGoingOrderList newInstance(String param1, String param2) {
        OnGoingOrderList fragment = new OnGoingOrderList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_on_going_order_list, container, false);
        preferenceManager = new PreferenceManager(getActivity());
        token = preferenceManager.getString(TOKEN_KEY);
        initView();

        makeGetOrderRequest();

        //On Swipe Refresh
        swipeRefresh();
        return view;
    }

    public void makeGetOrderRequest() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);

            // Initialize a new JsonArrayRequest instance
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, AcceptedOrdersRequestFormRestaurants, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("PresentOrder", response.toString());
                    try {
                        // Loop through the array elements
                        JSONObject jsonResult = new JSONObject(response.toString());
                        JSONArray jasonData = jsonResult.getJSONArray("data");
                        my_order_modelList = new ArrayList<>();
                        for (int i = 0; i < jasonData.length(); i++) {
                            // JSONArray data = response.getJSONArray("data");
                            JSONObject data1 = jasonData.getJSONObject(i);

                            String tempId = data1.getString("id");
                            String tempOrderId = data1.getString("order_id");
                            String tempOrderNo = data1.getString("order_no");
                            String tempRestaurent = data1.getString("resturant");
                            String tempRestaurentAddress = data1.getString("resturant_address");
                            String tempCustomer = data1.getString("customer");
                            String tempCustomerAddress = data1.getString("customer_address");
                            String tempOtp = data1.getString("otp");
                            String tempCreated_at = data1.getString("created_at");
                            double resturant_lat = 0.0,resturant_lng= 0.0,customer_lat= 0.0,customer_lng= 0.0;
                            try {
                                resturant_lat = data1.getDouble("resturant_lat");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            try {
                                resturant_lng = data1.getDouble("resturant_lng");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            try {
                                customer_lat = data1.getDouble("customer_lat");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            try {
                                customer_lng = data1.getDouble("customer_lng");
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            String tempOrderstatus = "";
                            String tempOrderStatusId = "";
                            try {
                                try {
                                    tempOrderstatus = data1.getString("orderstatus");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                tempOrderStatusId = data1.getString("orderstatus_id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                           /* OrderListModelClass orderListModelClass = new OrderListModelClass();
                            orderListModelClass.setId(tempId);
                            orderListModelClass.setOrderId(tempOrderId);
                            orderListModelClass.setOrderNo(tempOrderNo);
                            orderListModelClass.setResturant(tempRestaurent);
                            orderListModelClass.setResturantAddress(tempRestaurentAddress);
                            orderListModelClass.setCustomer(tempCustomer);
                            orderListModelClass.setCustomerAddress(tempCustomerAddress);

                            acceptedOrderListArrayList.add(orderListModelClass);*/
                            MyPastOrderModel my_past_orderModel = new MyPastOrderModel();

                            my_past_orderModel.setCreated_at(tempCreated_at);
                            my_past_orderModel.setCustomerName(tempCustomer);
                            my_past_orderModel.setId(tempId);
                            my_past_orderModel.setOrder_id(tempOrderId);
                            my_past_orderModel.setOrder_number(tempOrderNo);
                            my_past_orderModel.setRestaurantAddress(tempRestaurentAddress);
                            my_past_orderModel.setRestaurantName(tempRestaurent);
                            my_past_orderModel.setOtp(tempOtp);
                            my_past_orderModel.setDelivery_address(tempCustomerAddress);
                            my_past_orderModel.setOrderStatus(tempOrderstatus);
                            my_past_orderModel.setOrderStatusId(tempOrderStatusId);
                            my_past_orderModel.setResturant_lat(resturant_lat);
                            my_past_orderModel.setResturant_lng(resturant_lng);
                            my_past_orderModel.setCustomer_lat(customer_lat);
                            my_past_orderModel.setCustomer_lng(customer_lng);

                            my_past_orderModel.setStatus("To be Delivered");
                            my_order_modelList.add(my_past_orderModel);

                            frame_layout_empty.setVisibility(View.GONE);

                        }
                        OnGoingOrderAdapter adapter = new OnGoingOrderAdapter(mContext,my_order_modelList);
                        rv_myorder.setAdapter(adapter);
                        adapter.notifyDataSetChanged();

//                       CurrentOrderListAdapter currentOrderListAdapter = new CurrentOrderListAdapter(acceptedOrderListArrayList);
//                        rvMyCurrentOrder.setAdapter(currentOrderListAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        frame_layout_empty.setVisibility(View.VISIBLE);
                        frame_layout_empty.setGravity(Gravity.CENTER);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                       // UImsgs.showToast(mContext, getResources().getString(R.string.connection_time_out));
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("X_AUTH_TOKEN", token);
                    return map;
                }
            };
       /* MyAdapter myAdapter=new MyAdapter(this,R.layout.list_view_array_adapter,time_Table);
        simpleList.setAdapter(myAdapter);*/
            requestQueue.add(jsonObjectRequest);
        }
        /*{
            String tag_json_obj = "json_socity_req";
            Map<String, String> params = new HashMap<String, String>();
            params.put("d_id", get_id);

            CustomVolleyJsonArrayRequest jsonObjReq = new CustomVolleyJsonArrayRequest(Request.Method.POST,
                    BaseURL.GET_DELIVERD_ORDER_URL, params, new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<MyPastOrderModel>>() {
                    }.getType();
                    my_order_modelList = gson.fromJson(response.toString(), listType);
                    OnGoingOrderAdapter adapter = new OnGoingOrderAdapter(my_order_modelList);
                    rv_myorder.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    if (my_order_modelList.isEmpty()) {
                        // Toast.makeText(getActivity(), getResources().getString(R.string.no_rcord_found), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


        }*/
    }

    private void initView() {
        rv_myorder = view.findViewById(R.id.rv_myorder);
        //rv_myorder.setLayoutManager(new LinearLayoutManager(getActivity()));
        //Swipe Refresh
        refresh_swipe_ongoing = view.findViewById(R.id.refresh_swipe_ongoing);
        refresh_swipe_ongoing.setColorSchemeResources(R.color.Green);

        // Linear Layout Frame
        frame_layout_empty = view.findViewById(R.id.frame_layout_empty);

        RecyclerView.LayoutManager mlayoutmanager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv_myorder.setHasFixedSize(false);
        rv_myorder.setLayoutManager(mlayoutmanager);
        rv_myorder.setItemViewCacheSize(10);
        rv_myorder.setDrawingCacheEnabled(true);
        rv_myorder.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

    }

    private void swipeRefresh() {
        refresh_swipe_ongoing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                my_order_modelList.clear();//Clear List
                makeGetOrderRequest();
                refresh_swipe_ongoing.setRefreshing(false);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /* @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onResume() {
        super.onResume();
        startTimer(60000);
    }

    private void startTimer(int duration){
        timer= new CountDownTimer(duration, 60000) {

            public void onTick(long millisUntilFinished) {
                makeGetOrderRequest();
            }

            public void onFinish() {
                startTimer(60000);
            }
        }.start();
    }
}


