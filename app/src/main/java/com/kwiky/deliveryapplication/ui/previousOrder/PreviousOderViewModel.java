package com.kwiky.deliveryapplication.ui.previousOrder;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PreviousOderViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PreviousOderViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is dashboard fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}