package com.kwiky.deliveryapplication.ui.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.constants.IConstant;
import com.kwiky.deliveryapplication.ui.MapTrack.MapMainActivity;
import com.kwiky.deliveryapplication.ui.MapTrack.MapsActivity;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.ActionThingsForOrderReceivedCompletedDelivery;

public class TrackFragment extends Fragment {
    Context mContext;
    View view;

    private NotificationsViewModel notificationsViewModel;

    //Text View Tool Bar
    TextView textToolbar;

    private Switch switchToolbar;

    String token;
    PreferenceManager preferenceManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel = ViewModelProviders.of(this).get(NotificationsViewModel.class);
        view = inflater.inflate(R.layout.fragment_track, container, false);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(getActivity());
        token = preferenceManager.getString(TOKEN_KEY);
        initView();

        switchChange();

        /*Intent intent = new Intent(mContext, MapsActivity.class);
        startActivity(intent);*/

        Intent intent = new Intent(mContext, MapMainActivity.class);
        startActivity(intent);

        return view;
    }

    private void switchChange() {
     /*   if (switchToolbar.isChecked()){
            Toast.makeText(mContext, "Toast", Toast.LENGTH_SHORT).show();
        }*/


        switchToolbar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (switchToolbar.isChecked()) {

                    HashMap<String, Object> data = new HashMap();
                    data.put(IConstant.param_delivery_boy_status, "1");
                    JSONObject json = new JSONObject(data);
                    changeDeliveryStatus(json);
                    Toast.makeText(mContext, "Checked", Toast.LENGTH_SHORT).show();

                    switchToolbar.setText("Active");
                } else {

                    Toast.makeText(mContext, "Un - Checked ", Toast.LENGTH_SHORT).show();
                    HashMap<String, Object> data = new HashMap();
                    data.put(IConstant.param_delivery_boy_status, "2");
                    JSONObject json = new JSONObject(data);
                    changeDeliveryStatus(json);
                    switchToolbar.setText("Active");
                }
            }
        });

        //switchToolbar.setTextOn("On"); // displayed text of the Switch whenever it is in checked or on state
        //switchToolbar.setTextOff("Off"); // displayed text of the Switch whenever it is in unchecked i.e. off state

    }

    private void changeDeliveryStatus(JSONObject json) {
        {
            final String data = json.toString();
            {
                RequestQueue requstQueue = Volley.newRequestQueue(mContext);

                StringRequest jsonobj = new StringRequest(Request.Method.POST, ActionThingsForOrderReceivedCompletedDelivery,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
//                            Toast.makeText(mContext, "Sucessfully Requested", Toast.LENGTH_SHORT).show();
                                    //LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_current_order_list_adapter, parent, false);

                                    LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View viewInflator = layoutInflater.inflate(R.layout.custom_toast, null);

                                    /*Toast.makeText(LoginActivity.this, "Message", Toast.LENGTH_SHORT).show();

                                    TextView constracts = viewInflator.findViewById(R.id.constracts);
                                    constracts.setText("Delivery Received ");
                                    //constracts.setTextColor();
                                    Toast toast = new Toast(mContext);
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setView(viewInflator);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL,0,0);
                                    toast.show();*/

                                    Snackbar.make(view, response, Snackbar.LENGTH_LONG)
                                            .setAction("OK", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    //Login(v);
                                                }
                                            }).show();
                                    JSONObject jsonResult = new JSONObject(response.toString());
                                    //String status = jsonResult.getString("status");
                                    //JSONArray roles = jsonResult.getJSONArray("Roles");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Toast.makeText(mContext, "Some Error Occured", Toast.LENGTH_SHORT).show();
                                Snackbar.make(view, IConstant.OOPS, Snackbar.LENGTH_LONG)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //Login(v);
                                            }
                                        }).show();
                            }
                        }
                ) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override

                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        params.put("X_AUTH_TOKEN", token);

                        return params;

                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return data == null ? null : data.getBytes("utf-8");

                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }


                    //here I want to post data to sever*/
                };
                jsonobj.setRetryPolicy(new DefaultRetryPolicy(500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requstQueue.add(jsonobj);


            }
        }

    }

    private void initView() {

        textToolbar = view.findViewById(R.id.text_toolbar);//Text View Tool Bar

        textToolbar.setText("Track Current Orders");

        //Switch
        switchToolbar = view.findViewById(R.id.switch_toolbar);
        switchToolbar.setVisibility(View.GONE);
    }
}