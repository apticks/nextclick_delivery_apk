package com.kwiky.deliveryapplication.ui.currentOrders;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CurrentOrderViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CurrentOrderViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}