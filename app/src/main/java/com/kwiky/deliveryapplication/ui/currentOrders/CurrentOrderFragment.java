package com.kwiky.deliveryapplication.ui.currentOrders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.LocationListener;
import com.google.android.material.tabs.TabLayout;
//import android.support.design.widget.TabLayout;
import com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs;
import com.kwiky.deliveryapplication.constants.IConstant;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.*;

public class CurrentOrderFragment extends Fragment implements LocationListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Switch switchToolbar;
    private ImageView profile;
    private Context mContext;
    private TextView textToolbar;
    private RelativeLayout r_layout;
    private View view;
    private CurrentOrderViewModel currentOrderViewModel;
    private String checked = "1";
    private String Unchecked = "2";
    String s1;
    String s2;
    private double latitudeChange, litutdeChange;

    Switch sb;
    private GpsTracker gpsTracker;
    String activity_status,token;

    PreferenceManager preferenceManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        currentOrderViewModel = ViewModelProviders.of(this).get(CurrentOrderViewModel.class);
        view = inflater.inflate(R.layout.fragment_current_order, container, false);
        mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);

        // Declare all component ids findViewById()
        iniView();

        activity_status = preferenceManager.getString("activity_status");
        try{
            if(activity_status.equalsIgnoreCase("online")){
                switchToolbar.setChecked(true);
                HashMap<String, Object> data = new HashMap();
                data.put(IConstant.param_delivery_boy_status, checked);
                //Toast.makeText(mContext, "Active", Toast.LENGTH_SHORT).show();
                //getLocation();
                JSONObject json = new JSONObject(data);
                changeDeliveryStatus(json, checked);
            }else{
                switchToolbar.setChecked(false);
                HashMap<String, Object> data = new HashMap();
                data.put(IConstant.param_delivery_boy_status, Unchecked);
                //Toast.makeText(mContext, "Not Active", Toast.LENGTH_SHORT).show();
                JSONObject json = new JSONObject(data);
                changeDeliveryStatus(json, checked);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        // Click Profile Icon Get Data
        profileClick();

        // Getting Delivery Person Status
        deliveryPersonStatus();

        // for giving Gps Permission
        TrackRealLocation();

        // get switch button Status
        switchChange();
        return view;
    }

    private void profileClick() {
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getting Profile Details
                profileFetcher();
            }
        });
    }

    private void profileFetcher() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, PROFILE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("MyResponse", "" + response.toString());

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean status = jsonObject.getBoolean("status");
                        String http = jsonObject.getString("http_code");
                        String message = jsonObject.getString("message");
                        if (status && http.equalsIgnoreCase("200") && message.equalsIgnoreCase("Success..!")) {

                            JSONObject data = jsonObject.getJSONObject("data");
                            String uniqueID = data.getString("unique_id");
                            String name = data.getString("first_name");
                            String email = data.getString("email");
                            String mobile = data.getString("phone");

                            LayoutInflater inflater = getLayoutInflater();
                            View alertLayout = inflater.inflate(R.layout.id_layout, null);
                            TextView name_tv = alertLayout.findViewById(R.id.prof_name),
                                    mobile_tv = alertLayout.findViewById(R.id.prof_mobile),
                                    mail_tv = alertLayout.findViewById(R.id.prof_mail),
                                    id_tv = alertLayout.findViewById(R.id.prof_ID);

                            name_tv.setText(name);
                            mobile_tv.setText(mobile);
                            mail_tv.setText(email);
                            id_tv.setText(uniqueID);
                            AlertDialog.Builder alert = new AlertDialog.Builder(mContext);

                            alert.setView(alertLayout);
                            alert.setCancelable(true);

                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }
                    } catch (Exception e) {
                        //Toast.makeText(mContext, e + "", Toast.LENGTH_SHORT).show();
                        Log.d("error", e + "");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Failed Response", "" + error);


                }
            }) {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("X_AUTH_TOKEN", token);

                    return map;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);


        }
    }

    private void deliveryPersonStatus() {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(mContext);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, DeliveryPersonStatusR, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("DeliveryPersonStatusR", "" + response.toString());
                    try {
                        // Loop through the array elements
                        JSONObject jsonResult = new JSONObject(response.toString());
                        //JSONObject jasonData = jsonResult.getJSONObject("data");
                        Log.e("Response In Json ", "" + String.valueOf(jsonResult));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        UImsgs.showToast(mContext, getResources().getString(R.string.connection_time_out));
                    } else {
                        Toast.makeText(mContext, String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("Content-Type", "application/json");
                    map.put("X_AUTH_TOKEN", token);
                    return map;
                }
            } /*@Override
            public byte[] getBody() throws AuthFailureError {
            try {
                return data == null ? null : data.getBytes("utf-8");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }*/;
       /* MyAdapter myAdapter=new MyAdapter(this,R.layout.list_view_array_adapter,time_Table);
        simpleList.setAdapter(myAdapter);*/
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(500000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }

    }

    private void switchChange() {
        switchToolbar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (switchToolbar.isChecked()) {
/*
                   switchToolbar.setChecked(true);
*/
                    HashMap<String, Object> data = new HashMap();
                    data.put(IConstant.param_delivery_boy_status, checked);
                    Toast.makeText(mContext, "Active", Toast.LENGTH_SHORT).show();
                    //getLocation();
                    JSONObject json = new JSONObject(data);
                    Log.e("DataChange", "" + json);
                    preferenceManager.putString("activity_status","online");
                    changeDeliveryStatus(json, checked);

                } else if (!switchToolbar.isChecked()) {
                    HashMap<String, Object> data = new HashMap();
                    data.put(IConstant.param_delivery_boy_status, Unchecked);
                    Toast.makeText(mContext, "Not Active", Toast.LENGTH_SHORT).show();
                    JSONObject json = new JSONObject(data);
                    Log.e("DataUnChange", "" + json);
                    preferenceManager.putString("activity_status","offline");
                    changeDeliveryStatus(json, Unchecked);
                }
            }
        });
    }

    private void changeDeliveryStatus(final JSONObject json, final String checked) {
        {
            final String data = json.toString();
            Log.e("Datanew", "" + data);
            {
                RequestQueue requstQueue = Volley.newRequestQueue(getActivity());
                StringRequest jsonobj = new StringRequest(Request.Method.POST, DeliveryPersonStatusU, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("DeliveryPersonStatusU", "" + response);
                        sb.setChecked(true);
                        //sendLatLong(json);
                        getLocation();
                        addTabs(viewPager);
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(mContext, "" + error, Toast.LENGTH_SHORT).show();
                            }
                        }
                ) {
                    @Override
                    public String getBodyContentType() {
                        return  "application/form-data";
//                        return "application/x-www-form-urlencoded; charset=UTF-8";
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("X_AUTH_TOKEN", token);
                        return params;
                    }

                    /*protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put(IConstant.param_delivery_boy_status, checked);
                        Log.e("Map", "" + params);
                        return params;
                    }*/
                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return data == null ? null : data.getBytes("utf-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                };
                jsonobj.setRetryPolicy(new DefaultRetryPolicy(0000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requstQueue.add(jsonobj);
            }
        }
    }

    private void iniView() {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        addTabs(viewPager);

        tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        textToolbar = view.findViewById(R.id.text_toolbar);//Text View Tool Bar

        textToolbar.setText("Current New Order");

        //Switch
        r_layout = view.findViewById(R.id.r_layout);
        switchToolbar = view.findViewById(R.id.switch_toolbar);

        sb = new Switch(getActivity());
        sb.setChecked(true);
        r_layout.addView(sb);

        //Profile
        profile = view.findViewById(R.id.profile);
    }

    private void addTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());//getFragmentManager() replace it by
        adapter.addFrag(new CurrentOrderList(getActivity()), getString(R.string.current_order));
        adapter.addFrag(new OnGoingOrderList(getActivity()), getString(R.string.present_order));

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location!=null){
            latitudeChange=location.getLatitude();
            litutdeChange=location.getLongitude();
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
            //super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
//        switchToolbar.setChecked(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        switchToolbar.setChecked(true);
    }

    // For Gps Permission
    private void TrackRealLocation() {
        try {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // for getting Current Location in Latitude or Longitude
    private void getLocation() {
        gpsTracker = new GpsTracker(getActivity());
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            Log.e("Lati", "" + String.valueOf(latitude));
            Log.e("Longi", "" + String.valueOf(longitude));

            s1 = String.valueOf(latitude);
            s2 = String.valueOf(longitude);

            HashMap<String, String> map = new HashMap<>();
            map.put("latitude", s1);
            map.put("longitude", s2);
            JSONObject json = new JSONObject(map);
            sendLatLong(json);

            Log.e("All", "" + s1 + " , " + s2);

        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    // call api for sending Latitude or Longitude on Server
    private void sendLatLong(JSONObject json) {
        final String data = json.toString();
        Log.e("sdva", "" + data);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SendLatLongstatus, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("LocationResponse", "" + response.toString());
                try {
                    JSONObject jsonResult = new JSONObject(response.toString());
                    String status = jsonResult.getString("status");
                    if (status.equals("true")) {
                        JSONObject jsonToken = new JSONObject(response.toString());
                        JSONObject jsonObjectData = jsonToken.getJSONObject("data");
                    } else {
                        Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Log.e("JsonError", "" + e);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VollyError", "" + error);
            }
        }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("X_AUTH_TOKEN", token);
              /*  params.put("latitude", s1);
                params.put("longitude", s2);
                Log.e("dascd", "" + s1 + "" + s2);*/
                return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
