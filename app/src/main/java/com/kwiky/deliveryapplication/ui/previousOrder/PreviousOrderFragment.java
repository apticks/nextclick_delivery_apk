package com.kwiky.deliveryapplication.ui.previousOrder;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.kwiky.deliveryapplication.Adapters.PreviousOrderAdapter;
import com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs;
import com.kwiky.deliveryapplication.PojoClass.MyPastOrderModel;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.constants.IConstant;
import com.kwiky.deliveryapplication.ui.currentOrders.CurrentOrderList;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.VolleyLog.TAG;
import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;
import static com.kwiky.deliveryapplication.constants.Urls.ActionThingsForOrderReceivedCompletedDelivery;
import static com.kwiky.deliveryapplication.constants.Urls.CompletedOrdersDetails;

public class PreviousOrderFragment extends Fragment {


    RecyclerView recyclePreviousOrder;
    //Context mContext;

    //Swipe Refresh
    SwipeRefreshLayout refresh_swipe_previous;

    List<MyPastOrderModel> myPreviousOrderModelList;

    private PreviousOderViewModel previousOderViewModel;
    View view;

    LinearLayout frame_layout_empty;

    //Text View Tool Bar
    TextView textToolbar;

    private Switch switchToolbar;
    String token;
    PreferenceManager preferenceManager;
    Context mContext;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        previousOderViewModel =
                ViewModelProviders.of(this).get(PreviousOderViewModel.class);
         view= inflater.inflate(R.layout.fragment_previous_order, container, false);
         mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        if(token==null){
            token = CurrentOrderList.token;
        }
        Log.d("token",token);
         initView();

         previousOrderAPI();

        switchChange();

         //On Swipe Up
        swipeUpRefresh();
        return view;
    }

    private void switchChange() {
     /*   if (switchToolbar.isChecked()){
            Toast.makeText(getActivity(), "Toast", Toast.LENGTH_SHORT).show();
        }*/


        switchToolbar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (switchToolbar.isChecked()){

                    HashMap<String, Object> data = new HashMap();
                    data.put(IConstant.param_delivery_boy_status, "1");
                    JSONObject json = new JSONObject(data);
                    changeDeliveryStatus(json);
                    Toast.makeText(getActivity(), "Active ", Toast.LENGTH_SHORT).show();

                    switchToolbar.setText("Active");
                }else{

                    Toast.makeText(getActivity(), "In Active", Toast.LENGTH_SHORT).show();
                    HashMap<String, Object> data = new HashMap();
                    data.put(IConstant.param_delivery_boy_status, "2");
                    JSONObject json = new JSONObject(data);
                    changeDeliveryStatus(json);

                    switchToolbar.setText("In - Active");
                }
            }
        });

        //switchToolbar.setTextOn("On"); // displayed text of the Switch whenever it is in checked or on state
        //switchToolbar.setTextOff("Off"); // displayed text of the Switch whenever it is in unchecked i.e. off state

    }

    private void changeDeliveryStatus(JSONObject json) {
        {

            final String data = json.toString();
            {
                RequestQueue requstQueue = Volley.newRequestQueue(getActivity());

                StringRequest jsonobj = new StringRequest (Request.Method.POST, ActionThingsForOrderReceivedCompletedDelivery,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                                try {
//                            Toast.makeText(getActivity(), "Sucessfully Requested", Toast.LENGTH_SHORT).show();
                                    //LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_current_order_list_adapter, parent, false);

                                    LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View viewInflator = layoutInflater.inflate(R.layout.custom_toast,null);

                                    //Toast.makeText(LoginActivity.this, "Message", Toast.LENGTH_SHORT).show();

                                  /*  TextView constracts = viewInflator.findViewById(R.id.constracts);
                                    constracts.setText("Delivery Received ");
                                    //constracts.setTextColor();
                                    Toast toast = new Toast(getActivity());
                                    toast.setDuration(Toast.LENGTH_SHORT);
                                    toast.setView(viewInflator);
                                    toast.setGravity(Gravity.CENTER_HORIZONTAL,0,0);
                                    toast.show();*/

                                    Snackbar.make(view, response, Snackbar.LENGTH_LONG)
                                            .setAction("OK", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    //Login(v);
                                                }
                                            }).show();
                                    JSONObject jsonResult = new JSONObject(response.toString());
                                    //String status = jsonResult.getString("status");
                                    //JSONArray roles = jsonResult.getJSONArray("Roles");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Toast.makeText(getActivity(), "Some Error Occured", Toast.LENGTH_SHORT).show();
                                Snackbar.make(view, IConstant.OOPS, Snackbar.LENGTH_LONG)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //Login(v);
                                            }
                                        }).show();

                            }
                        }

                ){
                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override

                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("X_AUTH_TOKEN", token);



                        return params;

                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return data == null ? null : data.getBytes("utf-8");

                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }


                    //here I want to post data to sever*/
                };
                jsonobj.setRetryPolicy(new DefaultRetryPolicy(500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requstQueue.add(jsonobj);


            }
        }
    }

    private void swipeUpRefresh() {
        refresh_swipe_previous.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Calling  Previous Order API
                previousOrderAPI();

                refresh_swipe_previous.setRefreshing(false);
            }
        });

    }

    private void previousOrderAPI() {

        {

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

            // Initialize a new JsonArrayRequest instance
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, CompletedOrdersDetails, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG+"previous orders",response.toString());

                    try {
                        // Loop through the array elements
                        JSONObject jsonResult = new JSONObject(response.toString());
                        JSONArray jasonData = jsonResult.getJSONArray("data");
                        if (String.valueOf(jasonData).equals("false")){
                            UImsgs.showToast(getActivity(), "No Data");
                        }else{
                            //UImsgs.showToast(getActivity(), "No Else");
                        }

                        myPreviousOrderModelList = new ArrayList<>();
                        for(int i=0;i<jasonData.length();i++) {
                            // JSONArray data = response.getJSONArray("data");
                            JSONObject data1 = jasonData.getJSONObject(i);
                            String tempId="";
                            try {
                                tempId = data1.getString("order_number");
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                            String tempRestaurant="";
                            try{
                                tempRestaurant = data1.getString("resturant");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            String tempResturantAddress = "";
                            try {
                                tempResturantAddress = data1.getString("resturant_address");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            String tempCustomerName = "";
                            try{
                                tempCustomerName = data1.getString("customer");
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            String tempPrice = "";
                            try {
                                tempPrice = data1.getString("price");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            //String tempCustomer = data1.getString("date");

                            MyPastOrderModel my_past_orderModel = new MyPastOrderModel();

                            my_past_orderModel.setId(tempId);
                            my_past_orderModel.setPrice(tempPrice);
                            my_past_orderModel.setRestaurantName(tempRestaurant);
                            my_past_orderModel.setRestaurantAddress(tempResturantAddress);
                            my_past_orderModel.setCustomerName(tempCustomerName);
                           /* my_past_orderModel.setTotal_amount(tempPrice);


                            my_past_orderModel.setStatus("To be Delivered");
                            my_past_orderModel.setNote("Note");
                            my_past_orderModel.setIs_paid("Not Paid");
                            my_past_orderModel.setTotal_kg("25");
                            my_past_orderModel.setSocity_id("31");
                            my_past_orderModel.setLocation_id("13");
                            my_past_orderModel.setDelivery_charge("50");
                            my_past_orderModel.setPayment_method("C.O.D");*/
                            myPreviousOrderModelList.add(my_past_orderModel);

                            frame_layout_empty.setVisibility(View.GONE);

                        }


                        PreviousOrderAdapter adapter = new PreviousOrderAdapter(myPreviousOrderModelList);

                        recyclePreviousOrder.setAdapter(adapter);

                        /*CurrentOrderListAdapter currentOrderListAdapter = new CurrentOrderListAdapter(acceptedOrderListArrayList);
                        rvMyCurrentOrder.setAdapter(currentOrderListAdapter);*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                        //Toast.makeText(getActivity(), String.valueOf(e), Toast.LENGTH_SHORT).show();
                        frame_layout_empty.setVisibility(View.VISIBLE);
                    }
                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        //Toast.makeText(getActivity(), getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                        UImsgs.showToast(getActivity(),getResources().getString(R.string.connection_time_out));
                    }

                }
            })
            {

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("X_AUTH_TOKEN", token);
                    Log.d("X_AUTH_TOKEN", token);

                    return map;
                }
            };
       /* MyAdapter myAdapter=new MyAdapter(this,R.layout.list_view_array_adapter,time_Table);
        simpleList.setAdapter(myAdapter);*/
            requestQueue.add(jsonObjectRequest);
        }

    }

    private void initView() {
        //Recycle View
        recyclePreviousOrder = view.findViewById(R.id.recycle_previous_order);

        //Swipe
        refresh_swipe_previous = view.findViewById(R.id.refresh_swipe_previous);
        refresh_swipe_previous.setColorSchemeResources(R.color.Green);

        // Linear Layout Frame
        frame_layout_empty = view.findViewById(R.id.frame_layout_empty);

        textToolbar = view.findViewById(R.id.text_toolbar);//Text View Tool Bar

        textToolbar.setText("Previous Order");

        //Switch
        switchToolbar = view.findViewById(R.id.switch_toolbar);
        switchToolbar.setVisibility(View.GONE);

        RecyclerView.LayoutManager mlayoutmanager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        recyclePreviousOrder.setHasFixedSize (false);
        recyclePreviousOrder.setLayoutManager (mlayoutmanager);
        recyclePreviousOrder.setItemViewCacheSize(10);
        recyclePreviousOrder.setDrawingCacheEnabled(true);
        recyclePreviousOrder.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //stop
    }
}