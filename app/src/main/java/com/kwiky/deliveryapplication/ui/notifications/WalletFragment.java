package com.kwiky.deliveryapplication.ui.notifications;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;
import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kwiky.deliveryapplication.ActivitiesActivity.LoginActivity.TOKEN_KEY;

public class WalletFragment extends Fragment {

    private ProgressDialog pDialog;
    SharedPreferences sharedPreferences;
    private LinearLayout main;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    static TextView walletBalance;
    private Context mContext;
    private String uniqueID, name, email, mobile, wallet ;
    private String URL = "http://cineplant.com/nextclick/user/master/user_details/";
    String token;
    PreferenceManager preferenceManager;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wallet, container, false);
         mContext = getActivity();
        preferenceManager = new PreferenceManager(mContext);
        token = preferenceManager.getString(TOKEN_KEY);
        preferenceManager = new PreferenceManager(mContext);
        walletBalance = (TextView) v.findViewById(R.id.walletBalance);
        main = (LinearLayout)v.findViewById(R.id.mainLayout);

        /*username = sharedPreferences.getString(TAG_USERNAME, null);
        email = sharedPreferences.getString(TAG_EMAIL, null);
        number = sharedPreferences.getString(TAG_MOBILE, null);
        if (balance != null) {
            walletBalance.setText("₹ " + balance);
        } else {
            walletBalance.setText("₹ 0");
        }*/

        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        profileFetcher();
        return v;
    }

    public void upadater(String amount) {
        profileFetcher();
        walletBalance.setText(amount);
    }

    public void profileFetcher() {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", "" + response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Boolean status = jsonObject.getBoolean("status");
                    String http = jsonObject.getString("http_code");
                    String message = jsonObject.getString("message");

                    if (status && http.equalsIgnoreCase("200")
                            && message.equalsIgnoreCase("Success..!")) {

                        JSONObject data = jsonObject.getJSONObject("data");
                        uniqueID = data.getString("unique_id");
                        name = data.getString("first_name");
                        email = data.getString("email");
                        mobile = data.getString("phone");
                         wallet = data.getString("wallet");

                        Log.e("Data", "\n" + uniqueID + "\n" + name + "\n" + email + "\n" + mobile);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("unique_id", uniqueID);
                        editor.putString("wallet", wallet);
                        editor.putString("name", name);
                        editor.commit();
                        walletBalance.setText(sharedPreferences.getString("wallet",null));
                    }
                } catch (Exception e) {
                    Log.d("error", e + "");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", error + "");
            }

        }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
//                map.put("X_AUTH_TOKEN", storedUserToken);
                map.put("X_AUTH_TOKEN", token);

                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new WithdrawFragment(), "Withdraw");
        adapter.addFragment(new TransactionsFragment(), "Transactions");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
