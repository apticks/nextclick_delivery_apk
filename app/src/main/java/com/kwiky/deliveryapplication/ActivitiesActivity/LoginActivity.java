package com.kwiky.deliveryapplication.ActivitiesActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.kwiky.deliveryapplication.Helpers.Validations;
import com.kwiky.deliveryapplication.Helpers.netwrokHelpers.ConnectionDetector;
import com.kwiky.deliveryapplication.constants.IConstant;
import com.kwiky.deliveryapplication.constants.IErrors;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;
import com.kwiky.deliveryapplication.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.kwiky.deliveryapplication.Helpers.uiHelpers.UImsgs.progressDialog;
import static com.kwiky.deliveryapplication.constants.Urls.ForgotPassword;
import static com.kwiky.deliveryapplication.constants.Urls.Login;
import static com.kwiky.deliveryapplication.constants.Urls.UserDetails;


public class LoginActivity extends AppCompatActivity {
    Context mContext;
    PreferenceManager preferenceManager;
    Validations validations;
    Animation animationShake;
    ConnectionDetector connectionDetector;
    Button signInButton;
    EditText userIdEditText, passwordEditText;
    private SharedPreferences sharedPreferences;

    private String uniqueID, name, wallet, Email;
    TextView register, forgot_password;
    public static final String TOKEN_KEY = "token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = getApplicationContext();
        preferenceManager = new PreferenceManager(mContext);
        validations = new Validations();
        animationShake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        connectionDetector = new ConnectionDetector(LoginActivity.this);
        sharedPreferences = getSharedPreferences("Delivery", Context.MODE_PRIVATE);
        preferenceManager = new PreferenceManager(LoginActivity.this);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDialog();
            }
        });

        //Action Bar Hide
        getSupportActionBar().hide();
        initView();
        signInButtonClick();

    }

    private void ShowDialog() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alertLayout = inflater.inflate(R.layout.layout_forgot_password, null);

        final EditText mailtext = alertLayout.findViewById(R.id.forgot_mail);
        final TextView ok = alertLayout.findViewById(R.id.ok);

        AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
        alert.setIcon(R.mipmap.ic_launcher);
        alert.setTitle("Forgot Password");
        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);
        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(true);
        final AlertDialog dialog = alert.create();
        dialog.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String forgotmail = mailtext.getText().toString().trim();
                if (forgotmail.length() == 0) {
                    mailtext.setError("Should Not Be Empty");
                    mailtext.requestFocus();
                } else if (validations.isValidEmail(forgotmail)) {
                    mailtext.setError("Inavalid");
                    mailtext.requestFocus();
                } else {
                    forgotPassword(forgotmail, dialog);

                }

            }
        });
    }

    public void forgotPassword(final String forgotMail, final AlertDialog adialog) {
        Map<String, String> datamap = new HashMap<>();
        datamap.put("identity", forgotMail);
        JSONObject json = new JSONObject(datamap);
        final String data = json.toString();
        adialog.dismiss();
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIcon(R.drawable.nextclick_logo_black);
        progressDialog.setMessage("Please wait while sending.....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ForgotPassword, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean status = jsonObject.getBoolean("status");
                        String message = jsonObject.getString("message");
                        if (status) {
                            progressDialog.dismiss();

                            AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage("Reset Link Has Been Sent To Your Mail");
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();

                        } else {
                            progressDialog.dismiss();
                            AlertDialog.Builder alert = new AlertDialog.Builder(LoginActivity.this);
                            alert.setIcon(R.mipmap.ic_launcher);
                            alert.setTitle("NextClick - Reply");
                            alert.setMessage(message);
                            alert.setCancelable(true);
                            AlertDialog dialog = alert.create();
                            dialog.show();
                        }

                    } catch (Exception e) {

                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", "" + error);
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);

    }

    private void signInButtonClick() {
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(view);
                /* */
            }
        });
    }

    private void login(View view) {
        if (isValidateFields()) {
            if (connectionDetector.isConnectingToInternet()) {
                String str_email = userIdEditText.getText().toString();
                String str_password = passwordEditText.getText().toString();

                //btn_login.setVisibility(View.INVISIBLE);
                //progressBar.setVisibility(View.VISIBLE);


                //loginDataVolley(str_email, str_password);

                HashMap<String, Object> data = new HashMap();
                data.put(IConstant.param_identity, str_email);
                data.put(IConstant.param_password, str_password);
                JSONObject json = new JSONObject(data);

                /*Map<String, Object> mainData = new HashMap<>();
                mainData.put("id", tempId);
                mainData.put("status", "2");*/

                loginPostData(json, view);

            } else {
                Snackbar.make(view, IConstant.internet_connnection, Snackbar.LENGTH_LONG)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Login(v);
                            }
                        }).show();
                Toast.makeText(LoginActivity.this, IConstant.internet_connnection, Toast.LENGTH_SHORT).show();
               /* Toast toast = new Toast(mContext);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setText(IConstant.internet_connnection);
                toast.setGravity(Gravity.CENTER_HORIZONTAL,0,0);
                toast.show();*/
            }
        }

    }

    private void loginPostData(JSONObject json, final View view) {
        {
            final String data = json.toString();
            {
                RequestQueue requstQueue = Volley.newRequestQueue(mContext);

                StringRequest jsonobj = new StringRequest(Request.Method.POST, Login, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                                    /*LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View viewInflator = layoutInflater.inflate(R.layout.custom_toast,null);*/

                            JSONObject jsonResult = new JSONObject(response);
                            String status = jsonResult.getString("status");

                            if (status.equals("true")) {

                                JSONObject jsonToken = new JSONObject(response.toString());
                                JSONObject jsonObjectData = jsonToken.getJSONObject("data");
                                //boolean b = jsonToken.getBoolean("");
                                String token = jsonObjectData.getString("token");
                                preferenceManager.putString(TOKEN_KEY, token);
                                Log.d("token",token);
                                profileFetcher(token);
                                Intent intent = new Intent(LoginActivity.this, com.kwiky.deliveryapplication.ActivitiesActivity.DashBoardActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Snackbar.make(view, status, Snackbar.LENGTH_LONG)
                                        .setAction("OK", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //Login(v);
                                            }
                                        }).show();
                            }
                            //JSONArray roles = jsonResult.getJSONArray("Roles");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //Toast.makeText(mContext, "Some Error Occured", Toast.LENGTH_SHORT).show();
                                Snackbar.make(view, IConstant.OOPS, Snackbar.LENGTH_LONG)
                                        .setAction("Retry", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                //Login(v);
                                            }
                                        }).show();
                            }
                        }
                ) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json";
                    }

                    @Override

                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json");
                        return params;
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        try {
                            return data == null ? null : data.getBytes("utf-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    }
                };
                jsonobj.setRetryPolicy(new DefaultRetryPolicy(500000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requstQueue.add(jsonobj);
            }
        }
    }

    private boolean isValidateFields() {
        if (validations.isBlank(userIdEditText)) {
            userIdEditText.startAnimation(animationShake);
            userIdEditText.setError(IErrors.EMPTY);
            userIdEditText.requestFocus();
            return false;
        } else if (validations.isValidEmail(userIdEditText.getText().toString())) {
            userIdEditText.startAnimation(animationShake);
            userIdEditText.setError(IErrors.EMAIL_INVALID);
            userIdEditText.requestFocus();
            return false;

        } else if (validations.isBlank(passwordEditText)) {
            passwordEditText.startAnimation(animationShake);
            passwordEditText.setError(IErrors.EMPTY);
            passwordEditText.requestFocus();
            return false;
        }
        /*else if (passwordEditText.getText().length()<2)
        {
            passwordEditText.startAnimation(animationLogo);
            passwordEditText.setError("Password should minimum 4 digit");
            passwordEditText.requestFocus();
            return false;
        }*/

        return true;
    }

    private void initView() {
        signInButton = findViewById(R.id.sign_in_button);
        userIdEditText = findViewById(R.id.user_id);
        passwordEditText = findViewById(R.id.password);
        register = findViewById(R.id.register);
        /*register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });*/
    }

    public void profileFetcher(final String token) {

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UserDetails, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", "" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Boolean status = jsonObject.getBoolean("status");
                    String http = jsonObject.getString("http_code");
                    String message = jsonObject.getString("message");

                    if (status && http.equalsIgnoreCase("200") && message.equalsIgnoreCase("Success..!")) {
                        JSONObject data = jsonObject.getJSONObject("data");
                        uniqueID = data.getString("unique_id");
                        name = data.getString("first_name");
                        wallet = data.getString("wallet");
                        Email = data.getString("email");

                        SharedPreferences.Editor editor = sharedPreferences.edit();

                        editor.putString("unique_id", uniqueID);
                        editor.putString("first_name", name);
                        editor.putString("wallet", wallet);
                        editor.putString("email", Email);

                        editor.commit();
                    }
                } catch (Exception e) {
                    Log.d("error", e + "");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", error + "");
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("X_AUTH_TOKEN", token);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);


    }

}
