package com.kwiky.deliveryapplication.ActivitiesActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kwiky.deliveryapplication.services.FoodJobDetailServices;
import com.kwiky.deliveryapplication.services.LocationService;
import com.kwiky.deliveryapplication.services.OngoingOrderService;
import com.kwiky.deliveryapplication.R;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class DashBoardActivity extends AppCompatActivity {
    ToggleButton toggleButtonStatus;
    BottomNavigationView navView;
    ConstraintLayout container;
    Context mContext;
    int i = 0;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_boarc);
        // Hide Supported Action BAR
        getSupportActionBar().hide();
        windowStatusColor();
        //Window Status Color
        showNavBar();
        // Navigation Bar
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_current_order, R.id.navigation_previous_order, R.id.navigation_track_now, R.id.navigation_show_profile, R.id.navigation_show_wallet)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        //Context
        mContext = getApplicationContext();
        initView();
        //setToggleText();
        swipeUP();
        FoodJobDetailServices foodJobDetailServices = new FoodJobDetailServices(DashBoardActivity.this);
    }
    private void swipeUP() {
        //container.
        //AHBottomNavigation aHBottomNavigation = new AHBottomNavigation();
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void windowStatusColor() {
        Window window = getWindow();
        window.setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        //window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(ContextCompat.getColor(DashBoardActivity.this, R.color.colorPrimary));
        }
    }
    private void setToggleText() {
        toggleButtonStatus.setText("Status");
        // Sets the text for when the button is first created.

        toggleButtonStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                // TODO Auto-generated method stub
                if (b) {
                    toggleButtonStatus.setTextOn("Available");
                    toggleButtonStatus.setBackgroundColor(Color.GREEN);
                    // Sets the text for when the button is in the checked state.*/
                } else {
                    toggleButtonStatus.setTextOff("Not Available");
                    // Sets the text for when the button is not in the checked state.
                    toggleButtonStatus.setBackgroundColor(Color.RED);

                }
            }
        });
        /*
        toggleButtonStatus.setText("Status");
        // Sets the text for when the button is first created.

        toggleButtonStatus.setTextOff("Not Available");
        toggleButtonStatus.setBackgroundColor(Color.RED);
        // Sets the text for when the button is not in the checked state.
        toggleButtonStatus.setTextOn("Available");
        toggleButtonStatus.setBackgroundColor(Color.GREEN);
        // Sets the text for when the button is in the checked state.*/
    }
    private void initView() {
        /*toggleButtonStatus = findViewById(R.id.toggle_button);*/
        navView = findViewById(R.id.nav_view);
        container = findViewById(R.id.container);
    }
    @Override
    protected void onResume() {
        super.onResume();
        showNavBar();
    }
    public void showNavBar() {
        View view = getWindow().getDecorView();
        view.setSystemUiVisibility(View.VISIBLE);
    }
    private void startServiseAll() {
        mContext.startService(new Intent(mContext, OngoingOrderService.class));
    }
    public void toastText() {
        //Lo
        Toast.makeText(mContext, "Toasting", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //startServiseAll();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //startServiseAll();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(mContext, LocationService.class));
        stopService(new Intent(mContext, FoodJobDetailServices.class));
        //Toast.makeText(mContext, "Destroy", Toast.LENGTH_SHORT).show();
    }

    private void runThread() {
        new Thread() {
            public void run() {
                while (i++ < 1000) {
                    try {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //btn.setText("#" + i);
                                //startServiseAll();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
