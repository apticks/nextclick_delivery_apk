package com.kwiky.deliveryapplication.ActivitiesActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.kwiky.deliveryapplication.R;
import com.kwiky.deliveryapplication.services.FoodJobDetailServices;
import com.kwiky.deliveryapplication.services.LocationService;
import com.kwiky.deliveryapplication.services.OngoingOrderService;
import com.kwiky.deliveryapplication.utilities.PreferenceManager;

public class SplashActivity extends AppCompatActivity {
    PreferenceManager preferenceManager;
    Context mContext;

    //Animation
    Animation animationLogo;

    //Image View
    ImageView imageLogo;
    TextView textDelivery;

    //Token
    String storedUserToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        mContext = getApplicationContext();

        preferenceManager = new PreferenceManager(getApplicationContext());
        storedUserToken = preferenceManager.getString(mContext.getResources().getString(R.string.token));

        initVierw();

        startServiseAll();

        //if (savedInstanceState == null) {
        animationLogo = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation);
        imageLogo.startAnimation(animationLogo);
        animationLogo = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.pro_animation);

        textDelivery.startAnimation(animationLogo);
        //}

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (storedUserToken != null) {

                    Intent intent = new Intent(SplashActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    //endSplash();
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    //Intent intent = new Intent(SplashActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 800);
    }


    private void endSplash() {
        animationLogo = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.logo_animation_back);
        imageLogo.startAnimation(animationLogo);


        animationLogo.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void initVierw() {
        imageLogo = findViewById(R.id.image_logo);
        textDelivery = findViewById(R.id.text_delivery);
    }

    private void startServiseAll() {
        //mContext.startService(new Intent(mContext, OngoingOrderService.class));
        mContext.startService(new Intent(mContext, LocationService.class));//Implicit Intent
        mContext.startService(new Intent(mContext, FoodJobDetailServices.class));
    }
}
